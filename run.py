from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.UI.Object_Branch_UI import Object_Branch
from Nan_Air.UI_API import UI_API

from Nan_Air.WindowV3.List_Window.List_Window import List_Window
from Nan_Air.WindowV3.Info_Window.Info_Window import Info_Window
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.LL_API import LL_API
import os
os.chdir(os.getcwd())

def main():
    UI_API.main_menu()

main()