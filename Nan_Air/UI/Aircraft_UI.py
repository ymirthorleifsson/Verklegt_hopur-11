# aircraft branch 3    ================
# Main aircraft window ---------------- XC UI complete
from Nan_Air.WindowV3.List_Window.Aircraft_List_Window import Aircraft_List_Window
from Nan_Air.WindowV3.Info_Window.Info_Window import Info_Window
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.LL_API import LL_API


class Aircraft_UI():

    aircraft_info_window = Info_Window("Aircraft Info Window", None)
    aircraft_add_window = Add_Window("Aircraft Add Window", Aircraft.get_info_names(), LL_API.store_aircraft)

    aircraft_list_window = Aircraft_List_Window("Aircrafts",
                                       Aircraft.get_header(),
                                       LL_API.get_aircraft_page,
                                       LL_API.get_number_of_aircraft,
                                       aircraft_add_window,
                                       aircraft_info_window,
                                       None)
    def run():
        Aircraft_UI.aircraft_list_window.run()