from Nan_Air.UI.Render import Render
from Nan_Air.LL_API import LL_API
import math
import os

from sys import platform

OBJECTS_PER_PAGE = 9

class Object_Branch():
    def __init__(self, object_id, object_header, object_info,  name):
        self.object_id = object_id
        self.object_header = object_header
        self.object_info = object_info
        self.name = name

    def get_id(self, item_info):
        pass

    def clear_screen():
        if platform == "linux" or platform == "linux2" or platform == "darwin":
            # Linux or macOS
            return os.system("clear")
   
        elif platform == "win32":
            # Windows
            return os.system("cls")

    def create_object(self, object_info):
        pass

    def edit_object(self, object_info):
        pass

    def get_number_of_objects(self, object_id, search):
        return LL_API.get_number_of_objects(object_id, search)

    def get_object_page(self, object_id, search, page, objects_per_page):
        return LL_API.get_object_page(object_id, search, page, objects_per_page)

    # Display functions ===============================================================================
    def display_list_window(self, header, object_list, name, page, max_pages):
        Object_Branch.clear_screen()
        Render.header(name, right="|B| Back")
        string_list = []
        for item in object_list:
            string_list += [item.get_list_info().split("#")]
        Render.display_list(header, string_list)
        button_names = ["|A| Add", "|N| Next Page", "|P| Previous Page", "|S| Search"]
        Render.tool_bar(button_names)
        Render.page_bar(page, max_pages)
        Render.the_border()

    def display_add_window(self, item_info, name, edit_index):
        Object_Branch.clear_screen()
        Render.header(name)
        Render.the_info_window(item_info, edit=True, edit_index=edit_index)
        button_names = ["|C| Confirm", "|U| Up", "|D| Down"]
        Render.tool_bar(button_names)
        Render.the_border()

    def display_info_window(self, item_info, name):
        Object_Branch.clear_screen()
        Render.header(name)
        Render.the_info_window(item_info)
        button_names = ["|E| Edit"]
        Render.tool_bar(button_names)
        Render.the_border()

    def display_edit_window(self, item_info, name, edit_index):
        Object_Branch.clear_screen()
        Render.header(name)
        Render.the_info_window(item_info, edit=True, edit_index=edit_index)
        button_names = ["|C| Confirm", "|U| Upp", "|D| Down"]
        Render.tool_bar(button_names)
        Render.the_border()

    # Driver functions =================================================================================
    def list_window(self):
        page = 1
        search = None
        while True:
            # Updating page info
            max_page = math.ceil(self.get_number_of_objects(search) / OBJECTS_PER_PAGE)
            object_page = self.get_object_page(search, page, OBJECTS_PER_PAGE)

            # Displaying window
            self.display_list_window(self.object_header, object_page, self.name, page, max_page)

            # Resolving user input
            user_input = Render.the_input().lower()
            if user_input ==  "n" and page < max_page:
                page += 1
            elif user_input == "p" and 1 < page:
                page -= 1
            elif user_input == "s":
                search = input("Search keyword: ")
                page = 1
            elif user_input == "a":
                self.add_window()

            # If info window was selected
            elif user_input.isdigit():
                if 1 <= int(user_input) <= len(object_page):
                    item_info = object_page[int(user_input) - 1].get_detailed_info()
                    self.info_window(item_info)
            elif user_input == "b":
                return 0

    def add_window(self):
        edit_index = 0
        new_object_data = [] + self.object_info

        for i, info_type in enumerate(new_object_data):
            new_object_data[i] = [info_type, ""]

        while True:
            # Preventing edits out of bounds
            if edit_index < 0:
                edit_index = 0
            elif edit_index > len(new_object_data) - 1:
                edit_index = len(new_object_data) - 1
            self.display_add_window(new_object_data, self.name, edit_index)
            user_input = Render.the_input()
            if len(user_input) == 1:
                if user_input in "uU":
                    edit_index -= 1
                elif user_input in "dD":
                    edit_index += 1
                elif user_input in "cC":
                    self.create_object(new_object_data)
                    return 0
                elif user_input in "bB":
                    return 0
                else:
                    new_object_data[edit_index][1] = user_input
                    edit_index += 1
            else:
                new_object_data[edit_index][1] = user_input
                edit_index += 1

    def info_window(self, item_info):
        while True:
            self.display_info_window(item_info, self.name)
            user_input = Render.the_input().lower()
            if user_input == "e":
                item_info = self.edit_window(item_info)
            elif user_input == "b":
                return 0

    def edit_window(self, item_info):
        edited_item_info = [] + item_info
        edit_index = 0
        while True:
            # Preventing edits out of bounds
            if edit_index < 0:
                edit_index = 0
            elif edit_index > len(edited_item_info) - 1:
                edit_index = len(edited_item_info) - 1

            # Display window
            self.display_edit_window(edited_item_info, self.name, edit_index)

            user_input = Render.the_input()
            if len(user_input) == 1:
                if user_input in "uU":
                    edit_index -= 1
                elif user_input in "dD":
                    edit_index += 1
                elif user_input in "cC":
                    self.edit_object(self.get_id(item_info), edited_item_info)
                    return edited_item_info
                elif user_input in "bB":
                    return item_info
                else:
                    edited_item_info[edit_index][1] = user_input
            else:
                edited_item_info[edit_index][1] = user_input
