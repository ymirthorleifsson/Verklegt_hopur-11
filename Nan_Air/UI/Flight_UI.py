from Nan_Air.WindowV3.List_Window.List_Window import List_Window
from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.WindowV3.Info_Window.Info_Window import Info_Window
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.WindowV3.Select_Window.Select_Window import Select_Window
from Nan_Air.Model_classes.Flight import Flight
from Nan_Air.LL_API import LL_API

class Flight_UI():
    flight_info_window = Info_Window("Flight Info", None)
    flight_list_window = List_Window("Flights", Flight.get_header(),
                                     LL_API.get_flight_page,
                                     LL_API.get_number_of_flights,
                                     None,
                                     flight_info_window,
                                     None)
    def run():
        Flight_UI.flight_list_window.run()