from Nan_Air.WindowV3.Info_Window.Employee_Info_Window import Employee_Info_Window
from Nan_Air.WindowV3.List_Window.Employee_List_Window import Employee_List_Window
from Nan_Air.WindowV3.List_Window.List_Window import List_Window
from Nan_Air.WindowV3.Edit_Window.Employee_Edit_Window import Employee_Edit_Window
from Nan_Air.WindowV3.Add_Window.Employee_Add_Window import Employee_Add_Window
from Nan_Air.WindowV3.Select_Window.String_Select_Window import String_Select_Window


from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.LL_API import LL_API

class Employee_UI():

    job_select_window = String_Select_Window("Select Job", "Job")
    licence_select_window = String_Select_Window("Select Licence", "Licence")
    rank_select_window = String_Select_Window("Select Rank", "Rank")

    employee_edit_window = Employee_Edit_Window("Employee Edit Window",
                                                job_select_window,
                                                licence_select_window,
                                                rank_select_window,
                                                LL_API.change_employee)
                                            
    employee_info_window = Employee_Info_Window("Employee Info Window", employee_edit_window)

    employee_add_window = Employee_Add_Window("Employee Add Window",
                                              Employee.get_info_names(),
                                              LL_API.store_employee,
                                              job_select_window,
                                              licence_select_window,
                                              rank_select_window)

    employee_list_window = Employee_List_Window("Employees",
                                       Employee.get_header(),
                                       employee_add_window,
                                       employee_info_window)
    def run():
        Employee_UI.employee_list_window.run()