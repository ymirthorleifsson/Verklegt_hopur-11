from Nan_Air.WindowV3.List_Window.List_Window import List_Window
from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.WindowV3.Info_Window.Info_Window import Info_Window
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.WindowV3.Select_Window.Select_Window import Select_Window

from Nan_Air.Model_classes.Destination import Destination
from Nan_Air.LL_API import LL_API

class Destination_UI():

    destination_edit_window = Edit_Window("Destination Edit Window", LL_API.change_destination)

    destination_info_window = Info_Window("Destination Info Window", destination_edit_window)
    destination_add_window = Add_Window("Destination Add Window", Destination.get_info_names(), LL_API.store_destination)

    destination_list_window = List_Window("Destinations",
                                       Destination.get_header(),
                                       LL_API.get_destination_page,
                                       LL_API.get_number_of_destinations,
                                       destination_add_window,
                                       destination_info_window,
                                       None)

    def run():
        Destination_UI.destination_list_window.run()