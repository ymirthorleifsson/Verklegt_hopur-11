from Nan_Air.WindowV3.List_Window.Voyage_List_Window import Voyage_List_Window
from Nan_Air.WindowV3.Add_Window.Add_Voyage_Window import Add_Voyage_Window
from Nan_Air.WindowV3.Info_Window.Voyage_Info_Window import Voyage_Info_Window
from Nan_Air.WindowV3.Edit_Window.Voyage_Edit_Window import Voyage_Edit_Window
from Nan_Air.WindowV3.Select_Window.Select_Window import Select_Window

from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Destination import Destination
from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.LL_API import LL_API
import math


class Voyage_UI():

    destination_select_window = Select_Window("Select Destination", Destination.get_header())
    aircraft_select_window = Select_Window("Select Aircraft", Aircraft.get_header())
    captain_select_window = Select_Window("Select Pilot", Employee.get_header())
    copilot_crew_select_window = Select_Window("Select Cabin Crew", Employee.get_header())
    flight_service_manager_select_window = Select_Window("Select Pilot", Employee.get_header())
    flight_attendant_select_window = Select_Window("Select Cabin Crew", Employee.get_header())

    voyage_edit_window = Voyage_Edit_Window("Voyage Edit Window",
                                            aircraft_select_window,
                                            captain_select_window,
                                            copilot_crew_select_window,
                                            flight_service_manager_select_window,
                                            flight_attendant_select_window)

    voyage_info_window = Voyage_Info_Window("Voyage Info Window", voyage_edit_window)

    voyage_add_window = Add_Voyage_Window("Add Voyage", destination_select_window)

    voyage_list_window = Voyage_List_Window("Voyage Window",
                                     Voyage.get_header(),
                                     voyage_add_window,
                                     voyage_info_window,
                                     ["all", "not finished", "finished", "manned", "unmanned"])

    def run():
        Voyage_UI.voyage_list_window.run()

