import datetime
LAYOVER_TIME_INT = 1
class Date_parser():
    def get_day(datetime_object):
        if isinstance(datetime_object, datetime.datetime):
            return datetime.datetime(datetime_object.year, datetime_object.month, datetime_object.day)
        else:
            1/0 #ekki datetime object

    def date_parser(date_string):
        """ Takes in a date string of format YYYY-MM-DD HH:MM:SS or YYYY-MM-DDTHH:MM:SS and returns a datetime object """
        date_string = date_string.replace("T", "-").replace(":", "-").replace(" ", "-")     # Replace all T, : and " " with - for the split function
        date_list = date_string.split("-")
        date_list = [int(i) for i in date_list]     # Change all values to intagers
        year, month, day, hour, minute = date_list[0], date_list[1], date_list[2], date_list[3], date_list[4]
        return datetime.datetime(year, month, day, hour, minute)


    def timedelta_parser(date_string):
        """ Takes in a string of format THH:MM:SS or HH:MM:SS and returns a timedelta object for adding to datetime """

        date_string = date_string.replace("T", "")
        hours_str, minutes_str, seconds_str = date_string.split(":")
        return datetime.timedelta(hours=int(hours_str), minutes=int(minutes_str), seconds=int(seconds_str))


    def get_arrival_times(departure_time_string, flight_time_string):
        """ Takes in a departure time string and a flight time string and returns 4 time objects.
            Departure time from BIRK, arrival time at Destination, Departure time from Destination and arrival time at BIRK
        """

        departure_time = Date_parser.date_parser(departure_time_string)
        flight_timedelta = Date_parser.timedelta_parser(flight_time_string)
        arrival_time = departure_time + flight_timedelta
        departure_2_time = arrival_time + datetime.timedelta(hours=LAYOVER_TIME_INT)
        arrival_2_time = departure_2_time + flight_timedelta
        return departure_time, arrival_time, departure_2_time, arrival_2_time


    def check_time_format(time_input_string):
        """ Takes in a date string and checsk if it is of correct format. Correct format: YYYY-MM-DD HH:MM:SS """

        correct_date_bool = False
        try:
            checked_date = datetime.datetime.strptime(time_input_string, "%Y-%m-%d %H:%M:%S")
            correct_date_bool = True
        except ValueError:
            correct_date_bool = False
        return correct_date_bool
