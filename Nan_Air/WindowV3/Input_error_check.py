from Nan_Air.Function_classes.Date_parser import Date_parser
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
import time
import datetime

def check_employee_input(user_input, edit_index):
    '''Checks input for employee'''

    # Check for comma
    if ',' in user_input:
        return False

    # Name
    if edit_index == 0:
        for letter in user_input:
            if letter.isalpha() == False and letter != ' ':
                return False
    
    # SSN
    elif edit_index == 1:
        if len(user_input) != 10 or user_input.isdigit() == False:
            return False
    
    # Phone number
    elif edit_index == 4:
        if user_input.isdigit() == False and user_input != '':
            return False

    # Email
    elif edit_index == 5:
        if user_input[-10:] != "@nanair.is" and user_input != '':
            return False

    return True

def check_aircraft_input(user_input, edit_index):
    '''Checks input for aircraft'''

    # Check for comma
    if ',' in user_input:
        return False

    # Capacity
    if edit_index == 2:
        for letter in user_input:
            if letter.isdigit() == False:
                return False

    return True

def check_destination_input(user_input, edit_index):
    '''Checks input for destination'''

    # Check for comma
    if ',' in user_input:
        return False

    # Country and airport
    if edit_index in [0,1]:
        for letter in user_input:
            if letter.isalpha() == False:
                return False
    
    # Code
    elif edit_index == 2:
        for letter in user_input:
            if letter.isalpha() == False or len(user_input) != 4 or letter.isupper() == False:
                return False

    # Flight time
    elif edit_index == 3:
        try:
            user_input = user_input.replace("T", "")
            a, b, c = user_input.split(":")
        except ValueError:
            return False
    
    # Distance from BIRK
    elif edit_index == 4:
        if user_input.isdigit() == False:
            return False

    # Contact name
    elif edit_index == 5:
        for letter in user_input:
            if letter.isalpha() == False and letter != ' ':
                return False

    # Contact number
    elif edit_index == 6:
        for digit in user_input:
            if digit.isalpha() or digit == ',':
                return False

    return True

def check_time_input(user_input, only_future, detail):
    '''Checks input for any time.'''
    try:
        user_input = user_input.split()
        day = user_input[0].split("-")
        if detail == 'minute':
            minute = user_input[1].split(":")
            new_date = datetime.datetime(int(day[0]), int(day[1]), int(day[2]), int(minute[0]), int(minute[1]))
        else:
            new_date = datetime.datetime(int(day[0]), int(day[1]), int(day[2]))
    except:
        Error_Window.run("Invalid date")
    else:
        if only_future:
            if datetime.datetime.now() <= new_date:
                return new_date
            else:
                Error_Window.run("Invalid date")