from Nan_Air.WindowV3.Window import Window
from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.WindowV3.Input_error_check import check_destination_input
from Nan_Air.WindowV3.Input_error_check import check_aircraft_input


BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down"]
class Add_Window(Window):
    def __init__(self, header, info_names, store_item_func):
        self.header = header
        self.info_names = info_names
        self.store_item_func = store_item_func

    def display(self, new_item_info, edit_index, buttons):
        Render.clear()
        Render.header(self.header)
        info_list = []
        for i in range(len(new_item_info)):
            if new_item_info[i] is None:
                info_list += [[self.info_names[i], "+"]]
            else:
                info_list += [[self.info_names[i], new_item_info[i]]]
        Render.the_info_window(info_list, True, edit_index)
        Render.tool_bar(buttons)
        Render.the_border()


    def run(self):
        new_item_info = []
        for _ in range(len(self.info_names)):
            new_item_info += [""]
        edit_index = 0
        while True:
            self.display(new_item_info, edit_index, BUTTONS)
            user_input = self.get_input()
            # User goes back
            if user_input.lower() == "b":
                return 0
            # User confirms the information
            elif user_input.lower() == "c":
                self.store_item_func(new_item_info)
                return 0
            # User wants to edit the next info (arrow goes down)
            elif user_input.lower() == "d":
                if edit_index != len(self.info_names) - 1:
                    edit_index += 1
            # User wants to edit the previous info (arrow goes up)
            elif user_input.lower() == "u":
                if edit_index != 0:
                    edit_index -= 1
            # User inputs information
            else:
                # Checks if the user is adding an aircraft or a destination
                if len(new_item_info) == 4:
                    error_check_func = check_aircraft_input(user_input, edit_index)
                else:
                    error_check_func = check_destination_input(user_input, edit_index)
                # Check for errors in user_input
                if error_check_func:
                    new_item_info[edit_index] = user_input
                    if edit_index != len(self.info_names) - 1:
                        edit_index += 1
                else:
                    Error_Window.run("Invalid information")
