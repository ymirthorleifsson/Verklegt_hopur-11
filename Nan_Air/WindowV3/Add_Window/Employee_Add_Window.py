from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.WindowV3.Select_Window.String_Select_Window import String_Select_Window
from Nan_Air.WindowV3.Render import Render
from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.LL_API import LL_API
from Nan_Air.WindowV3.Input_error_check import check_employee_input

INDEX_OF_JOB = 2
INDEX_OF_RANK = 6

BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down", "|ENTER| Add"]
class Employee_Add_Window(Add_Window):
    def __init__(self, header, info_names, store_item_func, job_select_window, licence_select_window, rank_select_window):
        self.header = header
        self.info_names = info_names
        self.store_item_func = store_item_func
        self.job_select_window= job_select_window
        self.licence_select_window = licence_select_window
        self.rank_select_window = rank_select_window

    def run(self):
        new_item_info = []
        for i in range(len(self.info_names)):
            new_item_info += [""]

        new_item_info[INDEX_OF_JOB] = None
        new_item_info[INDEX_OF_RANK] = None
        edit_index = 0
        while True:
            self.display(new_item_info, edit_index, BUTTONS)
            user_input = self.get_input()
            # User goes back
            if user_input.lower() == "b":
                return 0
            # User confirms the information
            elif user_input.lower() == "c":
                self.store_item_func(new_item_info)
                return 0
            # User wants to edit the next info (arrow goes down)
            elif user_input.lower() == "d":
                if edit_index != len(self.info_names) - 1:
                    edit_index += 1
            # User wants to edit the previous info (arrow goes up)
            elif user_input.lower() == "u":
                if edit_index != 0:
                    edit_index -= 1
            # User inputs information
            else:
                if edit_index == INDEX_OF_JOB:
                    possible_jobs = Employee.get_job_types()
                    job = self.job_select_window.run(possible_jobs)
                    if job is not None:
                        if job == "pilot":
                            possible_aircraft_licences = Aircraft.get_model_types()
                            aircraft_licence = self.licence_select_window.run(possible_aircraft_licences)
                            if aircraft_licence is not None:
                                job = job + ":" + aircraft_licence
                                new_item_info[INDEX_OF_JOB] = job
                                new_item_info[INDEX_OF_RANK] = None

                        elif job == "cabin_crew":
                            new_item_info[INDEX_OF_JOB] = job
                            new_item_info[INDEX_OF_RANK] = None

                elif edit_index == INDEX_OF_RANK:
                    if new_item_info[INDEX_OF_JOB] is not None:

                        possible_ranks = Employee.get_rank_types(new_item_info[INDEX_OF_JOB])
                        rank = self.rank_select_window.run(possible_ranks)
                        if rank is not None:
                            new_item_info[INDEX_OF_RANK] = rank
                    else:
                        Error_Window.run("you need to select a job before you select rank")
                else:
                    # Check for errors in user_input
                    if check_employee_input(user_input, edit_index):
                        new_item_info[edit_index] = user_input
                        if edit_index != len(self.info_names) - 1:
                            edit_index += 1
                    else:
                        Error_Window.run("Invalid information")
