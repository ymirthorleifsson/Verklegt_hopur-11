from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.WindowV3.Select_Window.Select_Time_Window import Select_Time_Window
from Nan_Air.WindowV3.Render import Render
from Nan_Air.LL_API import LL_API


DESTINATION_INDEX = 0
DEPARTURE_TIME_INDEX = 1
BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down", "|ENTER| Add"]
class Add_Voyage_Window(Add_Window):

    def store_item_func(self, item_info):
        LL_API.store_new_voyage(item_info[DESTINATION_INDEX].get_id(), item_info[DEPARTURE_TIME_INDEX])

    def __init__(self, header, destination_select_window):
        self.header = header
        self.destination_select_window = destination_select_window
        self.info_names = ["Destination", "Departure Time:"]

    def display(self, new_item_info, edit_index, buttons):
        Render.clear()
        Render.header(self.header)
        info_list = []
        if new_item_info[0] is None:
            info_list += [[self.info_names[0], str(new_item_info[1])]]
        else:
            info_list += [[self.info_names[0], new_item_info[0].get_airport()]]

        info_list += [[self.info_names[1], str(new_item_info[1])]]
        Render.the_info_window(info_list, True, edit_index)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self):
        new_item_info = [None, "+"]
        edit_index = 0

        while True:
            self.display(new_item_info, edit_index, BUTTONS)
            user_input = self.get_input()
            if user_input == "":
                if edit_index == DESTINATION_INDEX:
                    all_available_destinations = LL_API.get_available_destinations()
                    new_destination = self.destination_select_window.run(all_available_destinations)
                    if new_destination is not None:
                        new_item_info[DESTINATION_INDEX] = new_destination
                        edit_index += 1
                elif edit_index == DEPARTURE_TIME_INDEX:
                    time = Select_Time_Window.run(True, "minute")
                    if time is not None:
                        new_item_info[DEPARTURE_TIME_INDEX] = time
            elif user_input.lower() == "b":
                return 0
            elif user_input.lower() == "c":
                self.store_item_func(new_item_info)
                return 0
            elif user_input.lower() == "d":
                if edit_index != len(self.info_names) - 1:
                    edit_index += 1
                else:
                    edit_index = 0
            elif user_input.lower() == "u":
                if edit_index != 0:
                    edit_index -= 1
                else:
                    edit_index = len(self.info_names) - 1
            else:
                if edit_index == DEPARTURE_TIME_INDEX:
                    new_item_info[DEPARTURE_TIME_INDEX] = user_input
                    if edit_index != len(self.info_names) - 1:
                        edit_index += 1
                    else:
                        edit_index = 0
