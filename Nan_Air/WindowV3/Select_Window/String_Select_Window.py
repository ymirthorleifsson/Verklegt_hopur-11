from Nan_Air.WindowV3.Select_Window.Select_Window import Select_Window
from Nan_Air.WindowV3.Render import Render
import math

STRINGS_PER_PAGE = 9
BUTTONS = ["|N| Next Page", "|P| Previous Page", "|S| Search"]
class String_Select_Window(Select_Window):
    def __init__(self, header, string_type):
        self.header = header
        self.string_type = string_type

    def display(self, string_page, page, max_page, buttons):
        Render.clear()
        Render.header(self.header)
        string_page_render_format = []
        for string_a in string_page:
            string_page_render_format += [[string_a]]
        Render.display_list([self.string_type], string_page_render_format)
        Render.page_bar(page, max_page)
        Render.tool_bar(buttons)
        Render.the_border()


    def run(self, string_list):
        string_page = []
        page = 1
        max_page = 1
        search = ""
        while True:
            string_page = []
            for string_a in string_list:
                if search.lower() in string_a.lower():
                    string_page += [string_a]
            max_page = math.ceil(len(string_page)/STRINGS_PER_PAGE)

            if len(string_page) == 0:
                max_page = 1

            string_page = string_list[(page-1)*STRINGS_PER_PAGE:page*STRINGS_PER_PAGE]
            self.display(string_page, page, max_page, BUTTONS)

            user_input = self.get_input()
            if user_input.lower() == "b":
                return None
            elif user_input.lower() == "n":
                if page < max_page:
                    page += 1
            elif user_input.lower() == "p":
                if page > 1:
                    page -= 1
            elif user_input.lower() == "s":
                search = input("Search: ")
                page = 0
            elif user_input.isdigit():
                if 1 <= int(user_input) <= len(string_page):
                    return string_page[int(user_input) - 1]
