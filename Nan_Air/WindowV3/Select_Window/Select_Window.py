from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Window import Window
from Nan_Air.LL_API import LL_API
import math

OBJECTS_PER_PAGE = 9
BUTTONS = ["|N| Next Page", "|P| Previous Page", "|S| Search"]
class Select_Window(Window):
    def __init__(self, header, list_header):
        self.header = header
        self.list_header = list_header

    def display(self, item_page, page, max_page):
        Render.clear()
        Render.header(self.header)
        item_info_list = []
        for item in item_page:
            item_info_list += [LL_API.get_list_info(item).split("#")]
        Render.display_list(self.list_header, item_info_list)
        Render.page_bar(page, max_page)
        Render.tool_bar(BUTTONS)
        Render.the_border()

    def run(self, item_list):
        search = ""
        page = 1
        max_page = 1
        item_page = []
        while True:
            # Update item_page
            item_page = []
            for item in item_list:
                if search.lower() in LL_API.get_list_info(item).lower():
                    item_page += [item]

            if len(item_page) == 0:
                max_page = 1
            max_page = math.ceil(len(item_page) / OBJECTS_PER_PAGE)

            item_page = item_page[(page - 1) * OBJECTS_PER_PAGE:page * OBJECTS_PER_PAGE]


            # Display and get input
            self.display(item_page, page, max_page)
            user_input = self.get_input()

            if user_input == "b":
                return None
            elif user_input.lower() == "n":
                if page < max_page:
                    page += 1
            elif user_input.lower() == "p":
                if page > 1:
                    page -= 1
            elif user_input.lower() == "s":
                search = input("Search keyword: ")
                page = 0
            elif user_input.isdigit():
                if 1 <= int(user_input) <= len(item_page):
                    return item_page[int(user_input) - 1]
