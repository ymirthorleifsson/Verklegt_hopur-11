from Nan_Air.WindowV3.Select_Window.Select_Window import Select_Window
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Input_error_check import check_time_input
import datetime

class Select_Time_Window():
    def display():
        Render.clear()
        Render.header("Select Time")
        Render.the_border()

    def run(only_future, detail="minute"):
        while True:
            Select_Time_Window.display()
            if detail == "minute":
                user_input = input("Enter date (YYYY-MM-DD HH:MM): ")
                if user_input.lower() == "b":
                    return None
                new_date = check_time_input(user_input, only_future, detail)
                return new_date

            elif detail == "day":
                user_input = input("Enter date (YYYY-MM-DD): ")
                if user_input.lower() == "b":
                    return None
                new_date = check_time_input(user_input, only_future, detail)
                return new_date
            else:
                1/0 # Bomb