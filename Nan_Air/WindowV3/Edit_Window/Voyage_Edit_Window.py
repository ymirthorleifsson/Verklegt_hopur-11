from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.LL_API import LL_API


AIRCRAFT_INDEX = 0
CAPTAIN_INDEX = 1
COPILOT_LIST_INDEX = 2
FLIGHT_SERVICE_MANAGER_INDEX = 3
FLIGHT_ATTENDANT_LIST_INDEX = 4

BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down", "|R| Remove", "|ENTER| Select"]
class Voyage_Edit_Window(Edit_Window):
    def __get_edit_info_item_and_type(edit_info, index):
        if index == AIRCRAFT_INDEX:
            return edit_info[AIRCRAFT_INDEX], "aircraft"

        elif index == CAPTAIN_INDEX:
            return edit_info[CAPTAIN_INDEX], "captain"

        elif index <= len(edit_info[COPILOT_LIST_INDEX]) + 1:
            return edit_info[COPILOT_LIST_INDEX][index-2], "copilot"

        elif index == len(edit_info[COPILOT_LIST_INDEX]) + 2:
            return edit_info[FLIGHT_SERVICE_MANAGER_INDEX], "flight_service_manager"

        else:
            return edit_info[FLIGHT_ATTENDANT_LIST_INDEX][index - 3 - len(edit_info[COPILOT_LIST_INDEX])], "flight_attendant"

    def __init__(self, header,
                 select_aircraft_window,
                 select_captain_window,
                 select_copilot_window,
                 select_flight_service_manager_window,
                 select_flight_attendant_window):
        self.header = header

        self.select_aircraft_window = select_aircraft_window
        self.select_captain_window = select_captain_window
        self.select_copilot_window = select_copilot_window
        self.select_flight_service_manager_window = select_flight_service_manager_window
        self.select_flight_attendant_window = select_flight_attendant_window

    def __filter_employees_by_rank(employees, rank):
        filtered_employees = []
        for employee in employees:
            if employee.get_rank() == rank:
                filtered_employees += [employee]
        return filtered_employees

    def __filter_pilots_by_licence(pilots, aircraft):
        filtered_pilots = []
        for pilot in pilots:
            if pilot.get_licence() == aircraft.get_model():
                filtered_pilots += [pilot]
        return filtered_pilots


    def display(self, edit_info, edit_index, buttons):
        Render.clear()
        Render.header(self.header)
        indent_display_info = []


        # formatting data for indent_info_function
        info_format_list = []
        size_of_list = 3 + len(edit_info[COPILOT_LIST_INDEX]) + len(edit_info[FLIGHT_ATTENDANT_LIST_INDEX])
        for i in range(size_of_list):
            item_selected, item_type_str = Voyage_Edit_Window.__get_edit_info_item_and_type(edit_info, i)
            if item_selected is None:
                info_format_list += [[item_type_str, "+"]]
            else:
                info_format_list += [[item_type_str, item_selected.get_name()]]

        # fixing arrow being on cabin crew or pilot header

        Render.the_info_window(info_format_list, edit=True, edit_index=edit_index)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self, voyage):
        edit_index = 0
        #[aircraft, captain, [copilots], flight_service_manager, [flight_attendant]]
        edit_info = LL_API.get_edit_info(voyage)
        aircraft = edit_info[AIRCRAFT_INDEX]
        captain = edit_info[CAPTAIN_INDEX]
        copilot_list = edit_info[COPILOT_LIST_INDEX] + [None]
        flight_service_manager = edit_info[FLIGHT_SERVICE_MANAGER_INDEX]
        flight_attendant_list = edit_info[FLIGHT_ATTENDANT_LIST_INDEX] + [None]

        original_crew_list_with_none = [captain] + copilot_list + [flight_service_manager] + flight_attendant_list
        original_crew_list = []
        for employee in original_crew_list_with_none:
            if employee is not None:
                original_crew_list += [employee]

        original_aircraft_list = []
        if aircraft is not None:
            original_aircraft_list = [aircraft]

        available_employees = LL_API.get_available_employees(voyage.get_departure_time()) + original_crew_list
        available_aircraft = LL_API.get_available_aircraft(voyage.get_departure_time()) + original_aircraft_list
        for employee in available_employees:
            if employee.get_job() == "pilot":
                print(employee.get_licence())
            else:
                print()

        while True:
            edit_info = [aircraft, captain, copilot_list, flight_service_manager, flight_attendant_list]
            self.display(edit_info, edit_index, BUTTONS)
            user_input = self.get_input()
            item_selected, item_type_str = Voyage_Edit_Window.__get_edit_info_item_and_type(edit_info, edit_index)

            size_of_list = 3 + len(edit_info[COPILOT_LIST_INDEX]) + len(edit_info[FLIGHT_ATTENDANT_LIST_INDEX])
            # back is pressed
            if user_input.lower() == "b":
                return 0

            # if down is pressed
            elif user_input.lower() == "d":
                # amount of items - 1
                if edit_index == size_of_list - 1:
                    # arrow wrapping
                    edit_index = 0
                else:
                    edit_index += 1

            # if upp is pressed
            elif user_input.lower() == "u":
                if edit_index == 0:
                    edit_index = size_of_list - 1
                else:
                    edit_index -= 1

            elif user_input.lower() == "r":
                if item_type_str == "aircraft":
                    aircraft = None
                    captain = None
                    copilot_list = [None]
                    flight_service_manager = None
                    flight_attendant_list = [None]

                elif item_type_str == "captain":
                    captain = None

                elif item_type_str == "copilot":
                    edit_info.remove(item_selected)

                elif item_type_str == "flight_service_manager":
                    flight_service_manager = None

                elif item_type_str == "flight_attendant":
                    edit_info.remove(item_selected)

            elif user_input.lower() == "":
                if item_selected is None:
                    if item_type_str == "aircraft":
                        new_aircraft = self.select_aircraft_window.run(available_aircraft)
                        if new_aircraft is not None:
                            aircraft = new_aircraft

                    elif aircraft is not None:
                        if item_type_str == "captain":
                            available_captains = Voyage_Edit_Window.__filter_employees_by_rank(available_employees, "captain")
                            available_captains = Voyage_Edit_Window.__filter_pilots_by_licence(available_captains, aircraft)
                            new_captain = self.select_captain_window.run(available_captains)
                            if new_captain is not None:
                                captain = new_captain

                        elif item_type_str == "copilot":
                            available_copilots = Voyage_Edit_Window.__filter_employees_by_rank(available_employees, "copilot")
                            available_copilots = Voyage_Edit_Window.__filter_pilots_by_licence(available_copilots, aircraft)
                            available_copilots = list(set(available_copilots) - set(copilot_list))
                            new_copilot = self.select_copilot_window.run(available_copilots)
                            if new_copilot is not None:
                                copilot_list[-1] = new_copilot
                                copilot_list += [None]

                        elif item_type_str == "flight_service_manager":
                            available_flight_service_managers = Voyage_Edit_Window.__filter_employees_by_rank(available_employees, "flight_service_manager")
                            new_flight_service_manager = self.select_flight_service_manager_window.run(available_flight_service_managers)
                            if new_flight_service_manager is not None:
                                flight_service_manager = new_flight_service_manager

                        elif item_type_str == "flight_attendant":
                            available_flight_attendants = Voyage_Edit_Window.__filter_employees_by_rank(available_employees, "flight_attendant")
                            available_flight_attendants = list(set(available_flight_attendants) - set(flight_attendant_list))
                            new_flight_attendant = self.select_flight_attendant_window.run(available_flight_attendants)
                            if new_flight_attendant is not None:
                                flight_attendant_list[-1] = new_flight_attendant
                                flight_attendant_list += [None]
                else:
                    Error_Window.run("select + to add new item to voyage")

            # if user confirms voyage
            elif user_input.lower() == "c":
                employees =   [edit_info[CAPTAIN_INDEX]]\
                            + edit_info[COPILOT_LIST_INDEX]\
                            + [edit_info[FLIGHT_SERVICE_MANAGER_INDEX]]\
                            + edit_info[FLIGHT_ATTENDANT_LIST_INDEX]
                employee_ids = []

                voyage.manned_bool = False
                if edit_info[CAPTAIN_INDEX] is not None:
                    if len(edit_info[COPILOT_LIST_INDEX]) > 1:
                        if edit_info[FLIGHT_SERVICE_MANAGER_INDEX]:
                            voyage.manned_bool = True

                for employee in employees:
                    if employee is not None:
                        employee_ids += [employee.get_id()]
                voyage_info = [
                    edit_info[AIRCRAFT_INDEX].get_id(),
                    voyage.get_destination_id(),
                    voyage.get_departure_time(),
                    voyage.flight1_number,
                    voyage.flight2_number,
                    employee_ids,
                    voyage.manned_bool,
                    voyage.id_int
                ]
                LL_API.change_voyage(voyage.get_id(), voyage_info)
                voyage.set_info(voyage_info)
                return 0


















'''
AIRCRAFT_INDEX = 0
COPILOT_LIST_INDEX = 2
FLIGHT_ATTENDANT_LIST_INDEX = 4

BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down", "|R| Remove", "|ENTER| Select"]
class Voyage_Edit_Window(Edit_Window):
    def __get_item_from_edited_info(index, edited_info):
        if index == 0:
            # aircraft
            return edited_info[AIRCRAFT_INDEX]
        elif index <= len(edited_info[PILOT_LIST_INDEX]):
            # pilot
            return edited_info[PILOT_LIST_INDEX][index - 1]
        else:
            # cabin crew
            return edited_info[CABIN_CREW_LIST_INDEX][index - 1 - len(edited_info[PILOT_LIST_INDEX])]

    def __init__(self, header, select_aircraft_window, select_pilot_window, select_cabin_crew_window):
        self.header = header
        self.select_aircraft_window = select_aircraft_window
        self.select_pilot_window = select_pilot_window
        self.select_cabin_crew_window = select_cabin_crew_window

    def display(self, edited_info, edit_index, buttons):

        Render.clear()
        Render.header(self.header)
        indent_display_info = []

        # formatting data for indent_info_function
        if edited_info[0] is None:
            indent_display_info += [["Aircraft", "+", 1]]
        else:
            indent_display_info += [["Aircraft", edited_info[AIRCRAFT_INDEX].get_name(), 1]]

        indent_display_info += [["Pilots", "", 1]]
        for pilot in edited_info[PILOT_LIST_INDEX]:
            if pilot is None:
                indent_display_info += [["+", "", 2]]
            else:
                indent_display_info += [[pilot.get_rank(), pilot.get_name(), 2]]

        indent_display_info += [["Cabin crew", "", 1]]
        for cc in edited_info[CABIN_CREW_LIST_INDEX]:
            if cc is None:
                indent_display_info += [["+", "", 2]]
            else:
                indent_display_info += [[cc.get_rank(), cc.get_name(), 2]]

        #fixing arrow being on cabin crew or pilot header
        arrow_pointer = edit_index
        if edit_index == 0:
            pass
        elif edit_index <= len(edited_info[PILOT_LIST_INDEX]):
            arrow_pointer += 1
        else:
            arrow_pointer += 2

        Render.indent_info_window(indent_display_info, edit=True, edit_index=arrow_pointer)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self, item):
        edit_index = 0
        # [aircraft, captain, [copilots], flight_service_manager, [flight_attendant]]
        edited_info = LL_API.get_edit_info(item)

        # Creating space for new copilot and flight_attendant
        edited_info[] += [None]
        edited_info[2] += [None]

        while True:
            self.display(edited_info, edit_index, BUTTONS)
            user_input = self.get_input()
            item_selected = Voyage_Edit_Window.__get_item_from_edited_info(edit_index, edited_info)

            size_of_list = 1 + len(edited_info[PILOT_LIST_INDEX]) + len(edited_info[CABIN_CREW_LIST_INDEX])
            # back is pressed
            if user_input.lower() == "b":
                return 0

            # if down is pressed
            elif user_input.lower() == "d":
                # amount of items - 1
                if edit_index == size_of_list- 1:
                    # arrow wrapping
                    edit_index  = 0
                else:
                    edit_index += 1

            # if upp is pressed
            elif user_input.lower() == "u":
                if edit_index == 0:
                    edit_index = size_of_list - 1
                else:
                    edit_index -= 1

            # if removed is pressed
            elif user_input.lower() == "r":
                if item_selected is None:
                    pass
                else:
                    if isinstance(item_selected, Aircraft):
                        edited_info[0] = None
                        edited_info[1] = [None]
                        edited_info[2] = [None]
                    else:
                        # item is employee
                        if item_selected.get_job() == "pilot":
                            edited_info[PILOT_LIST_INDEX].remove(item_selected)
                        elif item_selected.get_job() == "cabin_crew":
                            edited_info[CABIN_CREW_LIST_INDEX].remove(item_selected)

            # if user is trying to select new item
            elif len(user_input) == 0:
                if item_selected is None:
                    if edit_index == 0:
                        # if new aircraft was selected
                        list_of_all_available_aircraft = LL_API.get_available_aircraft(item.get_departure_time()) #reeeeeeeee
                        new_aircraft = self.select_aircraft_window.run(list_of_all_available_aircraft)
                        if new_aircraft is not None:
                            edited_info[AIRCRAFT_INDEX] = new_aircraft
                    else:
                        if edited_info[AIRCRAFT_INDEX] is not None:
                            if edit_index <= len(edited_info[PILOT_LIST_INDEX]):
                                # if new pilot was selected
                                list_of_all_available_pilots = LL_API.get_available_pilots(item.get_departure_time(), edited_info[AIRCRAFT_INDEX]) #reeee
                                new_pilot = self.select_pilot_window.run(list_of_all_available_pilots)
                                if new_pilot is not None:
                                    edited_info[PILOT_LIST_INDEX][-1] = new_pilot
                                    edited_info[PILOT_LIST_INDEX] += [None]
                            else:
                                # if new cabin crew was selected
                                list_of_all_available_cabin_crew = LL_API.get_available_cabin_crew(item.get_departure_time())  # reeeeeeeee
                                new_cc = self.select_cabin_crew_window.run(list_of_all_available_cabin_crew)
                                if new_cc is not None:
                                    edited_info[CABIN_CREW_LIST_INDEX][-1] = new_cc
                                    edited_info[CABIN_CREW_LIST_INDEX] += [None]
                        else:
                            Error_Window.run("you need to select a plane before you add more crew")

                else:
                    # if user did not select plus sign
                    pass
                    '''
