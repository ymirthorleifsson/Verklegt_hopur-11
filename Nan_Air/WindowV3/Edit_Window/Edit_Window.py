from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Window import Window
from Nan_Air.LL_API import LL_API
import os

INDEX_OF_INFO = 1
INDEX_OF_EDIT_PERMISSION_BOOL = 2

BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down"]
class Edit_Window(Window):
    def __init__(self, header, change_item_func):
        self.header = header
        self.change_item_func = change_item_func

    def display(self, edited_info, edit_index, buttons):
        Render.clear()
        Render.header(self.header)
        item_info = []
        for info in edited_info:
            item_info += [info[0:INDEX_OF_EDIT_PERMISSION_BOOL]]
        Render.the_info_window(item_info, True, edit_index)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self, item):
        edited_info = LL_API.get_edit_info(item)
        # Edit index must start on allowed index
        edit_index = 0
        while edited_info[edit_index][INDEX_OF_EDIT_PERMISSION_BOOL] == False:
            edit_index += 1
            if edit_index > len(edited_info) - 1:
                edit_index = 0

        while True:
            self.display(edited_info, edit_index, BUTTONS)
            user_input = self.get_input()

            if user_input.lower() == "b":
                return 0
            elif user_input.lower() == "c":
                new_info = []
                for info in edited_info:
                    new_info += [info[INDEX_OF_INFO]]
                item.set_info(new_info)
                self.change_item_func(item.get_id(), new_info)
                return 0

            elif user_input.lower() == "u":
                edit_index -= 1
                if edit_index < 0:
                    edit_index = len(edited_info) - 1
                while edited_info[edit_index][INDEX_OF_EDIT_PERMISSION_BOOL] == False:
                    edit_index -= 1
                    if edit_index < 0:
                        edit_index = len(edited_info) - 1

            elif user_input.lower() == "d":
                edit_index += 1
                if edit_index > len(edited_info) - 1:
                    edit_index = 0
                while edited_info[edit_index][INDEX_OF_EDIT_PERMISSION_BOOL] == False:
                    edit_index += 1
                    if edit_index > len(edited_info) - 1:
                        edit_index = 0

            else:
                edited_info[edit_index][INDEX_OF_INFO] = user_input
