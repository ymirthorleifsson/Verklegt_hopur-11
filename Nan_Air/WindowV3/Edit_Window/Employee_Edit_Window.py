from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.WindowV3.Input_error_check import check_employee_input

from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Voyage import Voyage

from Nan_Air.LL_API import LL_API
import os

INDEX_OF_INFO = 1
INDEX_OF_EDIT_PERMISSION_BOOL = 2

JOB_INDEX = 2
RANK_INDEX = 6

BUTTONS = ["|C| Confirm", "|U| Up", "|D| Down", "|R| Remove", "|ENTER| Select"]
class Employee_Edit_Window(Edit_Window):
    def __init__(self, header, select_job_window, select_licence_window, select_rank_window, change_item_func):
        self.header = header

        self.select_job_window = select_job_window
        self.select_rank_window = select_rank_window
        self.select_licence_window = select_licence_window
        self.change_item_func = change_item_func

    def display(self, edit_info, edit_index, buttons):
        Render.clear()
        Render.header(self.header)

        item_info = []
        for info in edit_info:
            item_info += [info[0:INDEX_OF_EDIT_PERMISSION_BOOL]]
        
        Render.the_info_window(item_info, True, edit_index)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self, item):
        edit_info = LL_API.get_edit_info(item)
        # Edit index must start on allowed index
        edit_index = 0
        while edit_info[edit_index][INDEX_OF_EDIT_PERMISSION_BOOL] == False:
            edit_index += 1
            if edit_index > len(edit_info) - 1:
                edit_index = 0

        while True:
            self.display(edit_info, edit_index, BUTTONS)
            user_input = self.get_input()

            # User goes back
            if user_input.lower() == "b":
                return 0
            # User moves arrow up
            elif user_input.lower() == "u":
                edit_index -= 1
                if edit_index < 0:
                    edit_index = len(edit_info) - 1
                while edit_info[edit_index][INDEX_OF_EDIT_PERMISSION_BOOL] == False:
                    edit_index -= 1
                    if edit_index < 0:
                        edit_index = len(edit_info) - 1
            # User moves arrow down
            elif user_input.lower() == "d":
                edit_index += 1
                if edit_index > len(edit_info) - 1:
                    edit_index = 0
                while edit_info[edit_index][INDEX_OF_EDIT_PERMISSION_BOOL] == False:
                    edit_index += 1
                    if edit_index > len(edit_info) - 1:
                        edit_index = 0
            # User removes info
            elif user_input.lower() == "r":
                if edit_index == JOB_INDEX:
                    edit_info[JOB_INDEX][INDEX_OF_INFO] = "+"
                    edit_info[RANK_INDEX][INDEX_OF_INFO] = "+"
                elif edit_index == RANK_INDEX:
                    edit_info[RANK_INDEX][INDEX_OF_INFO] = "+"
            # User goes to a select window
            elif user_input == "":
                if edit_index == JOB_INDEX:
                    jobs = Employee.get_job_types()
                    job = self.select_job_window.run(jobs)
                    edit_info[RANK_INDEX][INDEX_OF_INFO] = "+"
                    if job == "pilot":
                        licences = Aircraft.get_model_types()
                        licence = self.select_licence_window.run(licences)
                        job = job + ":" + licence
                        edit_info[edit_index][INDEX_OF_INFO] = job
                    elif job == "cabin_crew":
                        edit_info[edit_index][INDEX_OF_INFO] = job
                elif edit_index == RANK_INDEX:
                    if edit_info[JOB_INDEX][INDEX_OF_INFO] != "+":
                        ranks = Employee.get_rank_types(edit_info[JOB_INDEX][INDEX_OF_INFO])
                        rank = self.select_rank_window.run(ranks)
                        edit_info[edit_index][INDEX_OF_INFO] = rank
                    else:
                        Error_Window.run("you need to select a job before you select rank")
                    

            # User confirms changes
            elif user_input == "c":
                new_info = []
                for info in edit_info:
                    new_info += [info[INDEX_OF_INFO]]
                item.set_info(new_info)
                self.change_item_func(item.get_id(), new_info)
                return 0
            # User inputs info
            else:
                if edit_index != JOB_INDEX and edit_index != RANK_INDEX:
                    if check_employee_input(user_input, edit_index):

                        edit_info[edit_index][INDEX_OF_INFO] = user_input
                    else:
                        Error_Window.run("Invalid information")
