from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.List_Window.List_Window import List_Window

from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Employee import Employee

from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.WindowV3.Select_Window.Select_Time_Window import Select_Time_Window

from Nan_Air.LL_API import LL_API


class Work_Week_List(List_Window):
    ''' Displays the the working schedual for a week for a single employee.

    Attributes:
        header (str): The header for the window
        list_header (list): List of strings for the header of each row
    '''
    def __init__(self, header, list_header):
        '''

        Args:
            header (str): Header for the window
            list_header (list): Header for the columns.
        '''
        self.header = header
        self.list_header = list_header


    def __display_list(self, header_row = [], data_list = [], width = 120, border = "#"):
        '''Copied code from WindowDisplay.py to display the list with no select column.'''

        new_data_list = []
        
        # Finds max length of data
        data_max_list = [len(x) for x in data_list[0]]
        for line in data_list[1:]:
            for idx, item in enumerate(line):
                if len(item) > data_max_list[idx]:
                    data_max_list[idx] = len(item)

        # Calculations for correct window width
        header_max_list = [len(x) for x in header_row]
        length = sum(data_max_list) + 3 * (len(data_max_list) - 1) + 4
        header_length = sum(header_max_list) + 3 * (len(header_max_list) - 1) + 4

        while length <= width and length <= header_length:
            idx_to_add = 0
            max_change = 0
            for idx, item in enumerate(data_max_list):
                if header_max_list[idx] - data_max_list[idx] > max_change:
                    idx_to_add = idx
                    max_change = header_max_list[idx] - data_max_list[idx]

            data_max_list[idx_to_add] += 2
            length = sum(data_max_list) + 3 * (len(data_max_list) - 1) + 4

        x = 1
        while length <= width - 4:
            idx_to_add = x % len(data_max_list) - 1
            data_max_list[idx_to_add] += 2
            x += 1
            length = sum(data_max_list) + 3 * (len(data_max_list) - 1) + 4

        the_list = [header_row if idx == 0 else data_list[idx - 1] for idx, x in enumerate(range(len(data_list) + 1))]
        new_list = []

        for line in the_list:
            temp_list = []
            for idx, x in enumerate(line):
                temp_list.append("{:{align}{width}}".format(x, align="^", width=data_max_list[idx]))
            new_list.append(
                border + " " + "{:{align}{width}}".format(" | ".join(temp_list), align="<", width=width - 4) + " " + border)
            new_data_list = [x if idx < width - 1 else border for idx, x in enumerate(new_data_list[:width - 1])]
            new_list.append(border + "-" * (width - 2) + border)

        print(width * border)

        for line in new_list[:-1]:
            print(line)


    def display(self, work_week):
        Render.clear()
        Render.header(self.header)

        ymir_format = []
        for item in work_week:
            ymir_format += [[str(item[0]).split()[0], str(item[1])]]


        self.__display_list(self.list_header, ymir_format)

        # Render.display_list(self.list_header, ymir_format)

        Render.the_border()

    def run(self, work_week):
        while True:
            self.display(work_week)
            user_input = self.get_input()

            if user_input == "b":
                return 0