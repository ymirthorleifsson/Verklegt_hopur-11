from Nan_Air.WindowV3.Window import Window
from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Select_Window.Select_Time_Window import Select_Time_Window
from Nan_Air.LL_API import LL_API
import math
import datetime
import os

ITEMS_PER_PAGE = 9
BUTTONS = ["|S| Search", "|N| Next Page", "|P| Previous page", "|A| Add", "|T| Set Time Filter"]
class Voyage_List_Window(Window):
    def __init__(self, header, list_header, add_window, info_window, filters):
        self.header = header
        self.list_header = list_header
        self.add_window = add_window
        self.info_window = info_window
        self.filters = filters

    def display(self, item_list, page, max_page, list_filter, buttons):
        Render.clear()
        Render.header(self.header)
        if self.filters != None:
            filter_info_list = []
            for filt in self.filters:
                if filt == list_filter:
                    filter_info_list += ["# "+filt.upper()+" #"]
                else:
                    filter_info_list += [filt]
            Render.tool_bar(filter_info_list)

        item_info_list = []
        for item in item_list:
            item_info_list += [LL_API.get_list_info(item).split("#")]
        Render.display_list(self.list_header, item_info_list)
        Render.page_bar(page, max_page)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self):
        page = 1
        max_page = None
        search = None
        item_list = []
        list_filter = "all"
        time_filter = None

        if self.filters is None:
            buttons = [] + BUTTONS
        else:
            buttons = [] + BUTTONS + ["|F| Filter"]
        while True:
            number_of_items = LL_API.get_number_of_voyage_page_time(search, list_filter, time_filter)
            item_list = LL_API.get_voyage_page_time(search, page, ITEMS_PER_PAGE, list_filter, time_filter)


            max_page = math.ceil(number_of_items / ITEMS_PER_PAGE)
            if number_of_items == 0:
                max_page = 1
            self.display(item_list, page, max_page, list_filter, buttons)
            user_input = self.get_input()

            if user_input.lower() == "b":
                return 0
            elif user_input.lower() == "f":
                filter_buttons = []
                for filt in self.filters:
                    filter_buttons += ["|{}| {}".format(filt[0].upper(), filt[0].upper()+filt[1:])]
                self.display(item_list, page, max_page, list_filter, filter_buttons)
                user_input = self.get_input()
                for filt in self.filters:
                    if user_input.lower() == filt[0]:
                        list_filter = filt
                        page = 1
                if user_input.lower() == "b":
                    return 0
            elif user_input.lower() == "n":
                if page < max_page:
                    page += 1
            elif user_input.lower() == "p":
                if page > 1:
                    page -= 1
            elif user_input.lower() == "s":
                search = input("Search keyword: ")
                page = 1
            elif user_input.lower() == "a":
                self.add_window.run()
            elif user_input.lower() == "t":
                time = Select_Time_Window.run(True, "day")
                if time is not None:
                    delta_time = input("Enter how many days you want to check from previous entered date: ")
                    if delta_time.isdigit():
                        time_filter = time, time+datetime.timedelta(int(delta_time))

            elif user_input.isdigit():
                if 1 <= int(user_input) <= len(item_list):
                    item = item_list[int(user_input) - 1]
                    self.info_window.run(item)
