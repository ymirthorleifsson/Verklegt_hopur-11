from Nan_Air.WindowV3.Window import Window
from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Select_Window.Select_Time_Window import Select_Time_Window
from Nan_Air.LL_API import LL_API
import math
import os
import datetime

ITEMS_PER_PAGE = 9
BUTTONS = ["|S| Search", "|N| Next Page", "|P| Previous page", "|A| Add"]
class Aircraft_List_Window(Window):
    def __init__(self, header, list_header, get_item_list_func, get_number_of_items_func, add_window, info_window, filters):
        self.header = header
        self.list_header = list_header
        self.get_item_list_func = get_item_list_func
        self.get_number_of_items_func = get_number_of_items_func
        self.add_window = add_window
        self.info_window = info_window
        self.filters = filters

    def display(self, item_list, page, max_page, list_filter, buttons, time):
        Render.clear()
        Render.header(self.header)
        Render.tool_bar(["Showing aircrafts for time:  " + str(time)])

        item_info_list = []
        for item in item_list:
            item_info_list += [LL_API.get_list_info(item).split("#")]
        Render.display_list(self.list_header, item_info_list)
        Render.page_bar(page, max_page)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self):
        page = 1
        max_page = None
        search = None
        time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item_list = []
        list_filter = "all"
        buttons = [] + BUTTONS + ["|F| Filter"] + ["|C| Clear Filters"]
        while True:
            if self.filters == None:
                number_of_items = self.get_number_of_items_func(search)
                item_list = self.get_item_list_func(search, page, ITEMS_PER_PAGE)
            else:
                number_of_items = self.get_number_of_items_func(search, list_filter)
                item_list = self.get_item_list_func(search, page, ITEMS_PER_PAGE, list_filter)

            max_page = math.ceil(number_of_items / ITEMS_PER_PAGE)
            if number_of_items == 0:
                max_page = 1

            self.display(item_list, page, max_page, list_filter, buttons, time)
            user_input = self.get_input()

            if user_input.lower() == "c":
                time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                LL_API.set_filter_time(time)

            if user_input.lower() == "b":
                return 0
            elif user_input.lower() == "f":
                time = Select_Time_Window.run(True, "minute")
                if time is None:
                    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                LL_API.set_filter_time(str(time))
                
            elif user_input.lower() == "n":
                if page < max_page:
                    page += 1
            elif user_input.lower() == "p":
                if page > 1:
                    page -= 1
            elif user_input.lower() == "s":
                search = input("Search keyword: ")
                page = 1
            elif user_input.lower() == "a":
                self.add_window.run()
            elif user_input.isdigit():
                if 1 <= int(user_input) <= len(item_list):
                    item = item_list[int(user_input) - 1]
                    self.info_window.run(item)
