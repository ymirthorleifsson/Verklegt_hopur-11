from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.List_Window.List_Window import List_Window
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.WindowV3.Error_Window.Error_Window import Error_Window
from Nan_Air.WindowV3.Select_Window.Select_Time_Window import Select_Time_Window
from Nan_Air.LL_API import LL_API

import math

ITEMS_PER_PAGE = 9
BUTTONS = ["|S| Search", "|N| Next Page", "|P| Previous page", "|A| Add", "|F| Filter"]

class Employee_List_Window(List_Window):
    def __init__(self,
                 header,
                 list_header,
                 add_window,
                 info_window):
        self.header = header
        self.list_header = list_header
        self.add_window = add_window
        self.info_window = info_window
        self.filters = ["all", "pilots", "cabin crew", "working", "not working"]


    def display(self, item_list, page, max_page, list_filter, buttons, time):
        Render.clear()
        Render.header(self.header)
        filter_info_list = []
        for filt in self.filters:
            if filt == list_filter:             
                filter_info_list += ["# "+filt.upper()+" #"]
            else:
                filter_info_list += [filt]
        Render.tool_bar(filter_info_list)

        item_info_list = []
        if list_filter == "working":
            self.list_header = Employee.get_working_header()
            for item in item_list:
                item_info_list += [LL_API.get_working_list_info(item, time).split("#")]
        else:
            self.list_header = Employee.get_header()
            for item in item_list:
                item_info_list += [LL_API.get_list_info(item).split("#")]

        Render.display_list(self.list_header, item_info_list)
        Render.page_bar(page, max_page)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self):
        page = 1
        max_page = None
        search = None
        time = None
        item_list = []
        list_filter = "all"

        while True:
            number_of_items = LL_API.get_number_of_employees(search, list_filter, time)
            item_list = LL_API.get_employee_page(search, page, ITEMS_PER_PAGE, list_filter, time)


            max_page = math.ceil(number_of_items / ITEMS_PER_PAGE)
            if number_of_items == 0:
                max_page = 1
            self.display(item_list, page, max_page, list_filter, BUTTONS, time)
            user_input = self.get_input()

            if user_input.lower() == "b":
                return 0

            elif user_input.lower() == "f":
                filter_buttons = ["|A| all", "|P| pilots", "|C| cabin crew", "|W| working", "|N| not working"]
                self.display(item_list, page, max_page, list_filter, filter_buttons, time)
                user_input = self.get_input()

                if user_input.lower() == "w":
                    time = Select_Time_Window.run(True, detail="day")
                    if time is not None:
                        filter_time = time
                        list_filter = "working"

                elif user_input.lower() == "n":
                    time = Select_Time_Window.run(True, detail="day")
                    if time is not None:
                        filter_time = time
                        list_filter = "not_working"

                elif user_input.lower() == "a":
                    list_filter = "all"
                    page = 1

                elif user_input.lower() == "p":
                    list_filter = "pilot"
                    page = 1

                elif user_input.lower() == "c":
                    list_filter = "cabin_crew"
                    page = 1


            elif user_input.lower() == "n":
                if page < max_page:
                    page += 1
            elif user_input.lower() == "p":
                if page > 1:
                    page -= 1
            elif user_input.lower() == "s":
                search = input("Search keyword: ")
                page = 1
            elif user_input.lower() == "a":
                self.add_window.run()
            elif user_input.isdigit():
                if 1 <= int(user_input) <= len(item_list):
                    item = item_list[int(user_input) - 1]
                    self.info_window.run(item)