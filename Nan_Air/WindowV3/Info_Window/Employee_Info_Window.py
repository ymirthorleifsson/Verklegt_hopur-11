from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Window import Window
from Nan_Air.LL_API import LL_API
from Nan_Air.WindowV3.Select_Window.Select_Time_Window import Select_Time_Window
from Nan_Air.WindowV3.Select_Window.String_Select_Window import String_Select_Window
from Nan_Air.WindowV3.List_Window.Work_Week_List import Work_Week_List
from Nan_Air.Model_classes.Voyage import Voyage
import os

BUTTONS = ["|E| Edit", "|W| Work Week Overview"]


class Employee_Info_Window(Window):
    def __init__(self, header, edit_window):
        self.header = header
        self.edit_window = edit_window

    def display(self, item):
        Render.clear()
        Render.header(self.header)
        item_info = LL_API.get_info(item)
        Render.the_info_window(item_info)
        if self.edit_window != None:
            Render.tool_bar(BUTTONS)
        Render.the_border()

    def run(self, item):
        while True:
            self.display(item)
            user_input = self.get_input()
            if user_input.lower() == "b":
                return 0
            elif user_input.lower() == "e" and (self.edit_window != None):
                self.edit_window.run(item)

            elif user_input.lower() == "w":
                time = Select_Time_Window.run(True, "day")
                if time is not None:
                    work_week = LL_API.get_work_week(item, time)
                    ww_window = Work_Week_List("Work Week: {} for {}".format(str(time).split()[0], item.get_name()), ["Day", "Work Destination"])
                    ww_window.run(work_week)





