from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Info_Window.Info_Window import Info_Window
from Nan_Air.LL_API import LL_API

BUTTONS = ["|E| Edit"]

class Voyage_Info_Window(Info_Window):

    def display(self, item, buttons):
        Render.clear()
        Render.header(self.header)
        item_info = LL_API.get_info(item)
        Render.indent_info_window(item_info)
        Render.tool_bar(buttons)
        Render.the_border()

    def run(self, item):
        while True:
            self.display(item, BUTTONS)
            user_input = self.get_input()
            if user_input.lower() == "b":
                return 0
            elif user_input.lower() == "e" and (self.edit_window != None):
                self.edit_window.run(item)

