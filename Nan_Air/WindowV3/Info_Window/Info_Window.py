from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Window import Window
from Nan_Air.LL_API import LL_API
import os

BUTTONS = ["|E| Edit"]


class Info_Window(Window):
    def __init__(self, header, edit_window):
        self.header = header
        self.edit_window = edit_window

    def display(self, item):
        Render.clear()
        Render.header(self.header)
        item_info = LL_API.get_info(item)
        Render.the_info_window(item_info)
        if self.edit_window != None:
            Render.tool_bar(BUTTONS)
        Render.the_border()

    def run(self, item):
        while True:
            self.display(item)
            user_input = self.get_input()
            if user_input.lower() == "b":
                return 0
            elif user_input.lower() == "e" and (self.edit_window != None):
                self.edit_window.run(item)
