from platform import platform
import os

def short_word(word, length):
    if not word:
        return word
    if len(word) > length:
        return "".join(["." if x == length - 2 else word[-1] if x == length - 1 else word[x] for x in range(length)])
    else:
        return word


def the_header(width, border, left="", middle="", right=""):
    """
    Prints header with desired text and length in correct format.

    Args:
        width (int):  Width of header
        border (str): What border is made of
        left (str):   Left text in header
        middle (str): Center title in header
        right (str):  Right text in header

    Returns:
        None
    """

    # Header made out of desired str and with desired length, with height of 3
    header = [border * width for x in range(3)]
    # Length of word that fits into header
    length = (width - 4) // 3

    left = short_word(left, length)
    middle = short_word(middle, length)
    # If header is small, only command is displayed
    if len(right) >= length:
        right = right[:3]

    # Starting point of words
    start_first_print = 2
    start_middle_print = width // 2 - len(middle) // 2
    start_right_print = width - 2 - len(right)

    new_header = ""
    # Loops through each char in header
    for idx, char in enumerate(header[1]):
        # Skips first and last char (Keeping the border)
        if idx > 0 and idx < width - 1:
            # Prints out left word on correct place
            if idx >= start_first_print and idx <= len(left) + start_first_print - 1:
                new_header += left[idx - start_first_print]
                continue
            # Prints out middle word in the center
            if idx >= start_middle_print and idx <= len(middle) + start_middle_print - 1:
                new_header += middle[idx - start_middle_print]
                continue
            # Prints out right word on correct place
            if idx >= start_right_print and idx <= len(right) + start_right_print - 1:
                new_header += right[idx - start_right_print]
                continue
            new_header += " "
        else:
            new_header += char

    # Puts in new header
    header[1] = new_header
    for line in header[:2]:
        print(line)


def list_window(width, border, header_row=[], data_list=[]):
    """
    Prints out two dimensional list (array) in table format inside a window.

    Args:
        width (int):       Width of header
        border (str):      What border is made of
        header_row (list): List with collumn names
        data_list (list):  Array of data

    Returns:
        None

    """
    if not (header_row or data_list):
        return None

    empty = False
    if data_list == []:
        empty = True
    if empty:
        print(width * border)
        print(border + "{:^{width}}".format("Nothing to display", width=width - 2) + border)
        return
        # Add idx number to start of each data row
    new_data_list = []
    a_range = range(len(data_list))

    for idx in a_range:
        temp = ["|{}|".format(idx + 1)]
        if idx < len(data_list):
            for i in range(len(header_row)):
                temp.append(data_list[idx][i])
        else:
            for i in range(len(header_row)):
                temp.append("")
        new_data_list.append(temp)
    data_list = new_data_list

    # Add select to start of header row
    select = "Select"
    header_row = [select] + header_row

    # Finds max length of data
    data_max_list = [len(x) for x in data_list[0]]
    for line in data_list[1:]:
        for idx, item in enumerate(line):
            if len(item) > data_max_list[idx]:
                data_max_list[idx] = len(item)

    # Calculations for correct window width
    header_max_list = [len(x) for x in header_row]
    length = sum(data_max_list) + 3 * (len(data_max_list) - 1) + 4
    header_length = sum(header_max_list) + 3 * (len(header_max_list) - 1) + 4

    while length <= width and length <= header_length:
        idx_to_add = 0
        max_change = 0
        for idx, item in enumerate(data_max_list):
            if header_max_list[idx] - data_max_list[idx] > max_change:
                idx_to_add = idx
                max_change = header_max_list[idx] - data_max_list[idx]

        data_max_list[idx_to_add] += 2
        length = sum(data_max_list) + 3 * (len(data_max_list) - 1) + 4

    x = 1
    while length <= width - 4:
        idx_to_add = x % len(data_max_list) - 1
        data_max_list[idx_to_add] += 2
        x += 1
        length = sum(data_max_list) + 3 * (len(data_max_list) - 1) + 4

    # Shorten header row
    header_row = [short_word(x, data_max_list[idx]) for idx, x in enumerate(header_row)]

    the_list = [header_row if idx == 0 else data_list[idx - 1] for idx, x in enumerate(range(len(data_list) + 1))]
    new_list = []

    for line in the_list:
        temp_list = []
        for idx, x in enumerate(line):
            temp_list.append("{:{align}{width}}".format(x, align="^", width=data_max_list[idx]))
        new_list.append(
            border + " " + "{:{align}{width}}".format(" | ".join(temp_list), align="<", width=width - 4) + " " + border)
        new_data_list = [x if idx < width - 1 else border for idx, x in enumerate(new_data_list[:width - 1])]
        new_list.append(border + "-" * (width - 2) + border)

    print(width * border)

    for line in new_list[:-1]:
        print(line)


def info_window(width, border, the_list=[], edit=False, arrow_idx=0):
    """
    Prints out list in nice info and edit format inside a window.

    Args:
        width (int):     Width of header
        border (str):    What border is made of
        the list (list): 2D list to display, (variable and value)
        edit (bool):     Enables edit mode
        arrow_idx (int): Arrow index (What is being modified in edit)

    Returns:
        None
    """
    # Nr of rows
    rows = len(the_list) + 2

    # Remove ':' from variable names
    for idx, items in enumerate(the_list):
        if len(items) == 2:
            the_list[idx].append(0)
        the_list[idx][0] = " " * items[2] * 4 + items[0]

        if items[0][-1] == ":":
            the_list[idx][0] = items[0][:-1]

    # Find length for left and right text
    max_x = max_y = 0
    for row in the_list:
        if not row[1]:
            first, second = len(row[0]), 0
        else:
            first, second = len(row[0]), len(row[1])
        if first > max_x:
            max_x = first
        if second > max_y:
            max_y = second

    # Find text space
    splitter = " : "
    text_space = max_x + len(splitter) + max_y

    # Makes sure text fits into screenspace
    screen_space = width - 2
    while text_space > screen_space:
        if max_x > max_y:
            max_x -= 1
        else:
            max_y -= 1
        text_space = max_x + len(splitter) + max_y

    # Shorten text if it doesn't fit
    for i in range(len(the_list)):
        the_list[i] = [short_word(the_list[i][0], max_x), short_word(the_list[i][1], max_y)]

    print(width * border)
    # Prints out list
    for row in range(rows):
        if row != 0 and row != rows - 1:
            if the_list[row - 1][1] == None:
                string = "{:{align}{width_x}}   {:{align}{width_y}}".format(the_list[row - 1][0], "",
                                                                            align="<", width_x=max_x, width_y=max_y)
            else:
                string = "{:{align}{width_x}} : {:{align}{width_y}}".format(the_list[row - 1][0], the_list[row - 1][1],
                                                                            align="<", width_x=max_x, width_y=max_y)
            string = (border + "{:{align}{width}}".format(string, align="^", width=screen_space) + border)

            if edit and row - 1 == arrow_idx:
                arrow = "<----"
                start = 1 + text_space + (screen_space - text_space) // 2 + 1 - max_y + len(the_list[arrow_idx][1])
                for idx, char in enumerate(string):
                    if idx >= start and idx <= start + len(arrow) - 1:
                        print(arrow[idx - start], end="")
                    else:
                        print(char, end="")
                print()
                continue
            print(string)
        else:
            print(border + " " * screen_space + border)


def tool_bar(width, border, the_list):
    """
    Prints out bar with all tools in list

    Args:
        width (int):     Width of header
        border (str):    What border is made of
        the list (list): List to display

    Returns:
        None

    """
    # Calculates how much space each string has to display
    screen_space = width - 4
    space_for_each = (screen_space - len(the_list) - 1) // len(the_list)

    # Creates print list with all strings at same length that fits into window display
    print_list = []
    for item in the_list:
        print_list.append("{:^{width}}".format(short_word(str(item), space_for_each), width=space_for_each))

    # Prints Tool Bar
    print(width * border)
    print(border + " " + "{:^{width}}".format(" ".join(print_list), width=screen_space) + " " + border)


def page_bar(width, border, current_page, total_pages, message="Page:"):
    """
    Prints out page nr

    Args:
        width (int):        Width of header
        border (str):       What border is made of
        current_page (str): String of current page
        total_pages (str):  String of total pages
        message (str):      Message displayed

    Returns:
        None

    """
    # Changes int to str
    current_page, total_pages = str(current_page), str(total_pages)

    # Calculates where messages starts
    text_space = len(current_page) + 3 + len(total_pages)
    space_between = 1

    start_message = width // 2 - text_space // 2 - len(message) - space_between - text_space % 2
    message_range = range(start_message, start_message + len(message))

    # Creates Page nr. str
    screen_space = width - 4
    page_of = str(current_page) + " / " + str(total_pages)
    page_of = "{:^{width}}".format(page_of, width=screen_space)

    # Creates Page nr. str with message
    bar = border + " " + page_of + " " + border
    new_bar = [x if idx not in message_range else message[idx - start_message] for idx, x in enumerate(bar)]

    # Prints Page Bar
    print(width * border)
    print("".join(new_bar))


def main_menu(width, border, the_list=[]):
    """
        Prints out the main menu

        Args:
            width (int):     Width of main menu
            border (str):    What border is made of
            the list (list): List to display

        Returns:
            None
    """
    screen_space = width - 4
    max_length = max([len(str(x)) for x in the_list])
    menu_space = max_length * 2 + 3

    while menu_space > screen_space:
        max_length -= 2
        menu_space = max_length * 2 + 3

    if len(the_list) % 2 != 0:
        the_list.append("")

    the_list = [str(x) for x in the_list]
    the_list = [short_word(x, max_length) for x in the_list]

    print_list = []
    for idx in range(len(the_list) // 2):
        print_list.append(
            "{:<{width}}   {:<{width}}".format(the_list[2 * idx], the_list[2 * idx + 1], width=max_length))

    print(border * width)
    print(border + " " * (width - 2) + border)
    for item in print_list:
        print(border + " " + "{:^{width}}".format(item, width=screen_space) + " " + border)
    print(border + " " * (width - 2) + border)


def clear_screen():
    plat = platform().split("-")[0].lower()
    if plat == "linux" or plat == "linux2" or plat == "darwin":
        # Linux or macOS
        os.system("clear")

    elif plat == "windows":
        # Windows
        os.system("cls")


def error_window(width, border, message = "An Error has accured"):
    screen_space = width - 4
    error_message = message
    print(border * width)
    print(border + " " * (width - 2) + border)
    print(border + " {:^{width}} ".format(error_message, width = screen_space) + border)
    print(border + " {:^{width}} ".format("Press Enter to continue", width = screen_space) + border)
    print(border + " " * (width - 2) + border)


def big_info_window(width, border, the_list=[], the_header=[], edit=False, arrow_idx=0):
    """
    Prints out list in nice info and edit format inside a window.

    Args:
        width (int):     Width of header
        border (str):    What border is made of
        the list (list): 2D list to display, (variable and value)
        edit (bool):     Enables edit mode
        arrow_idx (int): Arrow index (What is being modified in edit)

    Returns:
        None
    """
    for i, sublist in enumerate(the_list):
        if len(sublist) != 2:
            the_list[i] = [sublist[0], ""]
    print(the_list)
    # Nr of rows
    rows = len(the_list) + 2

    # Remove ':' from variable names
    for idx, items in enumerate(the_list):
        if items[0][-1] == ":":
            the_list[idx][0] = items[0][:-1]

    # Find length for left and right text
    max_x = max_y = 0
    for row in the_list:
        first, second = len(row[0]), len(row[1])
        if first > max_x:
            max_x = first
        if second > max_y:
            max_y = second

    # Find text space
    splitter = " : "
    text_space = max_x + len(splitter) + max_y

    # Makes sure text fits into screenspace
    screen_space = width - 2
    while text_space > screen_space:
        if max_x > max_y:
            max_x -= 1
        else:
            max_y -= 1
        text_space = max_x + len(splitter) + max_y

    # Shorten text if it doesn't fit
    for i in range(len(the_list)):
        the_list[i] = [short_word(the_list[i][0], max_x), short_word(the_list[i][1], max_y)]

    print(width * border)
    # Prints out list
    for row in range(rows):
        if row != 0 and row != rows - 1:
            string = "{:{align}{width_x}} : {:{align}{width_y}}".format(the_list[row - 1][0], the_list[row - 1][1],
                                                                        align="<", width_x=max_x, width_y=max_y)
            string = (border + "{:{align}{width}}".format(string, align="^", width=screen_space) + border)

            if edit and row - 1 == arrow_idx:
                arrow = "<----"
                start = 1 + text_space + (screen_space - text_space) // 2 + 1 - max_y + len(the_list[arrow_idx][1])
                for idx, char in enumerate(string):
                    if idx >= start and idx <= start + len(arrow) - 1:
                        print(arrow[idx - start], end="")
                    else:
                        print(char, end="")
                print()
                continue
            print(string)
        else:
            print(border + " " * screen_space + border)


if __name__ == "__main__":
    info_window(50, "#", [["Header", None, 0],["Names","", 1],["1","Kristo", 2],["2","ymir", 2],["Ages","", 1], ["1", "19", 2]])
    info_window(50, "#", [["Header", ""],["Names",""],["1","Kristo"],["2","ymir"],["Ages",""], ["1", "19"]])
    info_window(50, "#", [["Header", ""],["Names",""],["1","Kristo"],["2","ymir"],["Ages",""], ["1", "19"]])
    # voyage_info_window(50, "#",
    #                    [["Header", "", 0], ["Names", "", 1], ["1", "Kristo", 2], ["2", "ymir", 2], ["Ages", "", 1],
    #                     ["1", "19", 2]])