from .WindowDisplay import *
import os

WIDTH = 120
BORDER = "#"
class Render():
    def header(name, right="|B| Back"):
        the_header(WIDTH, BORDER, left="Nan Air", middle=name, right=right)

    def display_list(header_row, data_list):
        list_window(WIDTH, BORDER, header_row, data_list)

    def display_buttons(buttons):
        tool_bar(WIDTH, BORDER, buttons)

    def the_border():
        print(BORDER * WIDTH)

    def the_input(message="Input: "):
        return input(message)

    def the_info_window(info_list, edit=False, edit_index=0):
        info_window(WIDTH, BORDER, info_list, edit, edit_index)

    def page_bar(page, max_pages):
        page_bar(WIDTH, BORDER, page, max_pages)

    def tool_bar(the_list):
        tool_bar(WIDTH, BORDER, the_list)
    
    def menu(the_list):
        main_menu(WIDTH, BORDER, the_list)

    def error(message):
        error_window(WIDTH, BORDER, message)

    def clear():
        #os.system("cls")
        clear_screen()

    def indent_info_window(the_list, edit=False, edit_index=0):
        info_window(WIDTH, BORDER, the_list, edit=edit, arrow_idx=edit_index)

