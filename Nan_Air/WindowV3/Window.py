from Nan_Air.WindowV3.Render import Render

class Window():
    def __init__(self, header):
        self.header = header

    def display(self):
        Render.header(self.header)
        Render.the_border()

    def get_input(self):
        return input("Input: ")

    def run(self):
        self.display()
