from Nan_Air.WindowV3.Render import Render
from Nan_Air.WindowV3.Window import Window

class Error_Window(Window):

    def display(error_message):
        Render.clear()
        Render.header("ERROR!")
        Render.error("Incorrect input")
        Render.the_border()

    def run(error_message):
        Error_Window.display(error_message)
        input()
        return 0

