from Nan_Air.Function_classes.Date_parser import Date_parser
import datetime

class Voyage():
    def __init__(self, aircraft_id, destination_id, departure_time, flight1_number, flight2_number, employee_ids, manned_bool, id_int):
        self.aircraft_id = aircraft_id
        self.destination_id = destination_id
        self.departure_time = Date_parser.date_parser(departure_time)
        self.flight1_number = flight1_number
        self.flight2_number = flight2_number
        self.employee_ids = employee_ids
        self.manned_bool = manned_bool
        self.id_int = id_int

    def get_data_list(self):
        return [self.aircraft_id, self.destination_id, self.departure_time.isoformat(), self.flight1_number, self.flight2_number, ";".join(str(x) for x in self.employee_ids), self.manned_bool, self.id_int]

    def get_header():
        return ["Destination","Departure Time","Aircraft","Pilots","Cabin Crew","Crew Status","Voyage Status"]

    def get_info_():
        pass
    
    def get_info_names(self):
        pass

    def get_list_info(self):
        if self.manned_bool == True:
            manned_status = "manned"
        else:
            manned_status = "unmanned"

        aircraft_name, destination_code, number_of_pilots, number_of_cabin_crew = Voyage_LL.get_id_info_for_list(self)
        return "{}#{}#{}#{}#{}#{}#{}".format(destination_code,
                                             self.departure_time,
                                             aircraft_name,
                                             number_of_pilots,
                                             number_of_cabin_crew,
                                             manned_status,
                                             "unfinished")

    def set_info(self, new_info):
        self.aircraft_id = new_info[0]
        self.destination_id = new_info[1]
        self.departure_time = new_info[2]
        self.flight1_number = new_info[3]
        self.flight2_number = new_info[4]
        self.employee_ids = new_info[5]
        self.manned_bool = new_info[6]
        self.id_int = new_info[7]

    def get_id(self):
        return self.id_int

    def get_destination_id(self):
        return self.destination_id

    def get_aircraft_id(self):
        if self.aircraft_id != None:
            return self.aircraft_id
        else:
            return None

    def get_employee_ids(self):
        return self.employee_ids

    def get_departure_time(self):
        return self.departure_time
    
    def get_departure_time_str(self):
        return self.departure_time.isoformat(timespec="seconds")

    def get_crew_status(self):
        if self.manned_bool:
            return "manned"
        else:
            return "unmanned"