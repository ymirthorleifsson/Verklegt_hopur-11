class Aircraft():
	ALL_MODELS = ["B757", "B767", "A320", "A380"]

	def __init__(self, model, name, capacity, manufacturer, unique_id):
		self.model = model		
		self.name = name
		self.capacity = capacity
		self.manufacturer = manufacturer
		self.unique_id = unique_id

	def get_model_types():
		return Aircraft.ALL_MODELS
		
	def get_name(self):
		return self.name

	def get_id(self):
		return self.unique_id

	def get_model(self):
		return self.model

	def get_header():
		return ["Name", "Model", "Capacity", "Manufacturer", "Status", "Flight info"]

	def get_info_names():
		"""Returns a list with detailed informations about the employee"""
		
		info_list = ["MODEL","NAME","CAPACITY","MANUFACTURER"]

		return info_list

	def set_info(self, new_info):
		self.model = new_info[0]
		self.name = new_info[1]
		self.capacity = new_info[2]
		self.manufacturer = new_info[3]
		self.unique_id = int(new_info[4])

	def get_data_list(self):
		"""Returns a list with the data from the Aircraft class"""
		return [self.model, self.name, self.capacity, self.manufacturer, self.unique_id]