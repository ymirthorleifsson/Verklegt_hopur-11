import datetime

class Flight():
    def __init__(self, flight_number_str, aircraft_str, departure_str, destination_str, departure_time, arrival_time, employees_str_list = []):
        self.flight_number_str = flight_number_str
        self.aircraft_str = aircraft_str
        self.departure_st = departure_str
        self.destination_str = destination_str
        self.departure_time = departure_time
        self.arrival_time = arrival_time
        self.employees_str_list = employees_str_list
        self.status = self.get_flight_status()

    def get_flight_status(self):
        if self.departure_time > datetime.datetime.now():
            return "Waiting"
        elif self.arrival_time < datetime.datetime.now():
            return "Completed"
        elif self.departure_time <= datetime.datetime.now() <= self.arrival_time:
            return "In flight"

    def get_header():
        """ Returns the header """
        # Used by list window and select window
        return ["Dept", "Dest", "Departure time", "Arrival time", "Status"]

    def get_flight_times(self):
        return self.departure_time, self.arrival_time
    
    def get_status(self):
        self.status = self.get_flight_status()
        return self.status
    
    def get_flight_times_str(self):
        return self.departure_time.isoformat(timespec="seconds"), self.arrival_time.isoformat(timespec="seconds")
