class Employee():
	ALL_JOBS_TYPES = ["pilot", "cabin_crew"]
	ALL_RANK_TYPES = {"pilot": ["captain", "copilot"],
					"cabin_crew": ["flight_attendant", "service_manager"]}

	def __init__(self, name, ssn, job, address, mobile_nr, email, rank, voyage_history_pointer = []):
		self.name = name
		self.ssn = ssn
		self.job = job
		self.address = address
		self.mobile_nr = mobile_nr
		self.email = email
		self.rank = rank
		self.voyage_history_pointer = voyage_history_pointer

	def get_job_types():
		# Used by add window and edit window (not yet implemented)
		return Employee.ALL_JOBS_TYPES

	def get_rank_types(job):
		if "pilot" in job:
			return Employee.ALL_RANK_TYPES["pilot"]
		else:
			return Employee.ALL_RANK_TYPES[job]


	def get_header():
		"""Returns the header"""
		# Used by list window and select window
		return ["Name", "Job", "Rank", "Email"]

	def get_working_header():
		"""Returns the header"""
		# Used by list window and select window
		return ["Name", "Job", "Rank", "Email", "Destination"]

	def get_info_names():
		"""Returns a list with detailed informations about the employee"""
		# Used by add window
		return ["NAME", "SSN", "JOB", "ADDRESS", "MOBILE NR", "EMAIL", "RANK"]


	def set_info(self, new_info):
		# Used by edit window
		self.name = new_info[0]
		self.ssn = int(new_info[1])
		self.job = new_info[2]
		self.address = new_info[3]
		self.mobile_nr = new_info[4]
		self.email = new_info[5]
		self.rank = new_info[6]

	def get_id(self):
		# Used by edit window and logic layer
		return self.ssn
	
	def get_job(self):
		# Used by logic layer filter
		if self.job[0:5].lower() == "pilot":
			return "pilot"
		else:
			return self.job.lower()

	def get_licence(self):
		return self.job[6:]

	def get_name(self):
		return self.name

	def get_rank(self):
		return self.rank

	def get_data_list(self):
		"""Returns a list with the data from the Employee class"""
		# Used by data layer
		return [self.name, self.ssn, self.job, self.address, self.mobile_nr, self.email, self.rank, self.voyage_history_pointer]
