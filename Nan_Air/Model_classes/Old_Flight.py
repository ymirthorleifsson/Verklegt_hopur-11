import datetime

class Old_Flight():
    def __init__(self, departure_airport_str, departure_time_str, destination_airport_str, arrival_time_str, plane_code_str, unique_id_str, flight_id_str, employees_list):
        self.departure_airport_str = departure_airport_str
        self.departure_time_str = departure_time_str
        self.destination_airport_str = destination_airport_str
        self.arrival_time_str = arrival_time_str
        self.plane_code_str = plane_code_str
        self.unique_id_str = unique_id_str
        self.flight_id_str = flight_id_str
        self.employees_list = employees_list

    def get_flight_info_for_voyage(self):
        return_info_list = [
            ["FLIGHT NUMBER: ", self.flight_id_str],
            ["UNIQUE ID: ", self.unique_id_str],
            ["DESTINATION: ", self.destination_airport_str],
            ["DEPARTURE: ", self.departure_airport_str],
            ["DEPARTURE TIME: ", self.departure_time_str],
            ["ARRIVAL TIME: ", self.arrival_time_str],
            ["PLANE: ", self.plane_code_str]
        ]
        return return_info_list

    def get_data_list(self):
        return [self.departure_airport_str, self.departure_time_str, self.destination_airport_str, self.arrival_time_str, self.plane_code_str, self.unique_id_str, self.flight_id_str, self.employees_list]