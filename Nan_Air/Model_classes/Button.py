class Button():
    def __init__(self, name, trigger_key, mapping_func):
        self.name = name
        self.trigger_key = trigger_key
        self.mapping_func = mapping_func

    def __str__(self):
        return "|{}| {}".format(self.trigger_key.upper(), self.name)

    def __len__(self):
        return 1