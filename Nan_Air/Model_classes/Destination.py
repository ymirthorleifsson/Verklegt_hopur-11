class Destination():
	def __init__(self, airport, code, flight_time, dist_from_birk, contact_name, contact_number, id_int, country_str):
		self.airport = airport
		self.code = code
		self.flight_time = flight_time
		self.dist_from_birk = dist_from_birk
		self.contact_name = contact_name
		self.contact_number = contact_number
		self.id = id_int
		self.country_str = country_str

	def get_header():
		return ["Country", "Airport", "Code", "Flight Time", "Distance from BIRK", "Contact", "Contact number"]

	def get_info_names():
		"""Returns a list with detailed informations about the destination"""

		return ["COUNTRY", "AIRPORT", "CODE", "FLIGHT TIME (HH:MM:SS)", "DISTANCE FROM BIRK", "CONTACT", "NUMBER"]


	def set_info(self, new_info):
		self.country_str = new_info[0]
		self.airport = new_info[1]
		self.code = new_info[2]
		self.flight_time = new_info[3]
		self.dist_from_birk = new_info[4]
		self.contact_name = new_info[5]
		self.contact_number = new_info[6]
		

	def get_id(self):
		return self.id

	def get_country(self):
		return self.country_str

	def get_data_list(self):
		return [self.airport, self.code, self.flight_time, self.dist_from_birk, self.contact_name, self.contact_number, self.id, self.country_str]

	def get_flight_time(self):
		return self.flight_time

	def get_code(self):
		return self.code

	def get_airport(self):
		return self.airport

	def get_dist_from_birk(self):
		return self.dist_from_birk
