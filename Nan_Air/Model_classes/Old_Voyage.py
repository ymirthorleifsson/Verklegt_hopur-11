import datetime
from Nan_Air.Function_classes.Date_parser import Date_parser
class Old_Voyage():
    def __init__(self, plane_str, destination_str, departure_1_time, arrival_1_time, departure_2_time, arrival_2_time, flight_1_number, flight_2_number, unique_id, employees_string_list = []):
        self.plane_str = plane_str
        self.destination_str = destination_str
        self.departure_1_time = departure_1_time
        self.arrival_1_time = arrival_1_time
        self.departure_2_time = departure_2_time
        self.arrival_2_time = arrival_2_time
        self.flight_1_number = flight_1_number
        self.flight_2_number = flight_2_number
        self.employees_string_list = employees_string_list
        self.unique_id = int(unique_id)

    def get_data_list(self):
        return [self.plane_str, self.destination_str, self.departure_1_time, self.arrival_1_time, self.departure_2_time, self.arrival_2_time,  self.flight_1_number, self.flight_2_number, ";".join(self.employees_string_list), self.unique_id]

    def get_header(self):
        return ["Destination","Departure Time","Aircraft","Pilots","Cabin Crew","Crew Status","Voyage Status"]

    def set_id(self, new_id):
        self.unique_id = int(new_id)

    def get_id(self):
        return self.unique_id

    def get_departure_time(self):
        return Date_parser.date_parser(self.departure_1_time)