
from Nan_Air.IO.Employee_IO import Employee_IO
from Nan_Air.IO.Voyage_IO import Voyage_IO
from Nan_Air.IO.Old_Voyage_IO import Old_Voyage_IO
from Nan_Air.IO.Destination_IO import Destination_IO
from Nan_Air.IO.Aircraft_IO import Aircraft_IO

class IO_API():
    EMPLOYEE_IO = Employee_IO()
    VOYAGE_IO = Voyage_IO()
    DESTIONATION_IO = Destination_IO()
    AIRCRAFT_IO = Aircraft_IO()
    OLD_VOYAGE_IO = Old_Voyage_IO()

    def get_data(class_id_str):
        """ Takes in a class id string and returns list of instances from file of given id string """
        if class_id_str == "employee":
            return IO_API.EMPLOYEE_IO.get_data()
        elif class_id_str == "voyage":
            return IO_API.VOYAGE_IO.get_data()
        elif class_id_str == "destination":
            return IO_API.DESTIONATION_IO.get_data()
        elif class_id_str == "aircraft":
            return IO_API.AIRCRAFT_IO.get_data()
        elif class_id_str == "old_voyage":
            return IO_API.OLD_VOYAGE_IO.get_data()

    def write_data(class_id_str, data_list):
        """ Takes in a class id string and a list of instances and overwrites the data list into file of given id string """
        if class_id_str == "employee":
            return IO_API.EMPLOYEE_IO.write_data(data_list)
        elif class_id_str == "voyage":
            return IO_API.VOYAGE_IO.write_data(data_list)
        elif class_id_str == "destination":
            return IO_API.DESTIONATION_IO.write_data(data_list)
        elif class_id_str == "aircraft":
            return IO_API.AIRCRAFT_IO.write_data(data_list)
        elif class_id_str == "old_voyage":
            return IO_API.OLD_VOYAGE_IO.write_data(data_list)

    def append_data(class_id_str, data_list):
        """ Takes in a class id string and a list of instances and appends the data list into file of given id string """
        if class_id_str == "employee":
            return IO_API.EMPLOYEE_IO.append_data(data_list)
        elif class_id_str == "voyage":
            return IO_API.VOYAGE_IO.append_data(data_list)
        elif class_id_str == "destination":
            return IO_API.DESTIONATION_IO.append_data(data_list)
        elif class_id_str == "aircraft":
            return IO_API.AIRCRAFT_IO.append_data(data_list)
        elif class_id_str == "old_voyage":
            return IO_API.OLD_VOYAGE_IO.append_data(data_list)
