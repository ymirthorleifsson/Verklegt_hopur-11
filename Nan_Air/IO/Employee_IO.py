from Nan_Air.Model_classes.Employee import Employee
import csv

class Employee_IO():
    data_id = "Nan_Air/Data/data_employee.csv"

    def get_data(self):
        """ Returns list of Employee instances from file """
        data_list = self.__read_data()
        employees_list = []
        for line in data_list:
            if line != data_list[0]:
                employee = Employee(line[0], int(line[1]), line[2], line[3], line[4], line[5], line[6], line[7])
                employees_list.append(employee)
        return employees_list

    def write_data(self, employees_list):
        """ Overwrites all data in file with new data """
        data_list = [["name", "ssn", "job", "address", "mobile_nr", "email", "rank", "flight_history"]]
        for employee in employees_list:
            data_list.append(employee.get_data_list())
        
        with open(self.data_id, "w", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def append_data(self, employees_list):
        """ Adds new data to data file """
        data_list = []
        for employee in employees_list:
            data_list += [employee.get_data_list()]
            
        with open(self.data_id, "a", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()
    
    def __read_data(self):
        """ Opens and reads in csv data file for reading """
        data_list = []
        file_stream = open(self.data_id, "r", encoding="utf-8")
        for line in file_stream:
            data_list.append(line.strip().split(","))
        file_stream.close()
        return data_list

#self, name, ssn, job, address, home_nr, mobile_nr, email, flight_history
#employee = Employee("Gudjon", "0112002570", "Pilot", "HR", "123", "A@Q.is", "123:123:123")
#emp = Employee_IO()
#emp.append_data(employee)
#emp = Employee_IO()
#emp.get_data()