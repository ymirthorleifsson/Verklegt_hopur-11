from Nan_Air.Model_classes.Aircraft import Aircraft
import csv

class Aircraft_IO():
    data_id = "Nan_Air/Data/data_aircraft.csv"

    def get_data(self):
        """ Returns list of aircraft instances from file """
        data_list = self.__read_data()
        aircraft_list = []

        for line in data_list:
            if line[0] != "Model" and line[1] != "Name":
                aircraft = Aircraft(line[0], line[1], line[2], line[3], int(line[4]))
                aircraft_list.append(aircraft)
        return aircraft_list

    def write_data(self, aircraft_list):
        """ Overwrites all data in file with new data """
        data_list = [["Model,Name", "Capacity", "Manufacturer", "Unique id"]]
        for aircraft in aircraft_list:
            data_list.append(aircraft.get_data_list())
        
        with open(self.data_id, "w", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def append_data(self, aircraft_list):
        """ Adds new data to data file """
        data_list = []
        for aircraft in aircraft_list:
            data_list.append(aircraft.get_data_list())
        
        with open(self.data_id, "a", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def __read_data(self):
        """ Opens and reads in csv data file for reading """
        data_list = []
        file_stream = open(self.data_id, "r")
        for line in file_stream:
            data_list.append(line.strip().split(","))
        file_stream.close()
        return data_list

#pl = aircraft_IO()
#aircraft = Airaircraft("Model", "Name", "Capacity", "Manufacturer", "Flight number")
#pl.write_data([aircraft])
#pl.append_data(aircraft)
#pl.get_data()