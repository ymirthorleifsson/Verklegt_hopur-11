from Nan_Air.Model_classes.Destination import Destination
import csv

class Destination_IO():
    data_id = "Nan_Air/Data/data_destination.csv"

    def get_data(self):
        """ Returns list of Destination instances from file """
        data_list = self.__read_data()
        destination_list = []

        for line in data_list:
            if line[0] != "Airport" and line[1] != "Code":
                destination = Destination(line[0], line[1], line[2], line[3], line[4], line[5], int(line[6]), line[7])
                destination_list.append(destination)
        return destination_list

    def write_data(self, destinations_list):
        """ Overwrites all data in file with new data """
        data_list = [["Airport", "Code", "Flight time", "Distance from BIRK", "Contact name", "Contact number", "ID", "Country"]]
        for destination in destinations_list:
            data_list.append(destination.get_data_list())
        
        with open(self.data_id, "w", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def append_data(self, destinations_list):
        """ Adds new data to data file """
        data_list = []
        for destination in destinations_list:
            data_list.append(destination.get_data_list())
        
        with open(self.data_id, "a", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def __read_data(self):
        """ Opens and reads in csv data file for reading """
        data_list = []
        file_stream = open(self.data_id, "r")
        for line in file_stream:
            data_list.append(line.strip().split(","))
        file_stream.close()
        return data_list

#des = Destination_IO()
#destination = Destination("Airport", "Code", "Flight time", "Distance from BIRK", "Number", "Contact name", "Contact number")
#des.write_data([destination])
#des.append_data(destination)
#des.get_data()
#airport, code, flight_time, dist_from_birk, number, contact_name, contact_number