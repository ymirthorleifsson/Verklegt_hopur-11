from Nan_Air.Model_classes.Voyage import Voyage
import csv

class Voyage_IO():
    data_id = "Nan_Air/Data/data_voyage.csv"

    def get_data(self):
        """ Returns list of Voyage instances from file """
        data_list = self.__read_data()
        voyages_list = []

        for line in data_list:
            if line != data_list[0]:
                manned_boolean = None
                if line[6] == "False" or line[6] == "FALSE":
                    manned_boolean = False
                elif line[6] == "True" or line[6] == "TRUE":
                    manned_boolean = True
                else:
                    1/0

                if line[0] != "":
                    voyage = Voyage(int(line[0]), int(line[1]), line[2], line[3], line[4], line[5], manned_boolean, int(line[7]))
                else:
                    voyage = Voyage(None, int(line[1]), line[2], line[3], line[4], line[5], manned_boolean,
                                    int(line[7]))
                voyages_list.append(voyage)
        return voyages_list

    def write_data(self, voyages_list):
        """ Overwrites all data in file with new data """
        data_list = [["Plane", "Destination", "Departure time", "Flight 1", "Flight 2", "Employee id list", "Manned bool", "Unique ID"]]
        for voyage in voyages_list:
            data_list.append(voyage.get_data_list())
        
        with open(self.data_id, "w", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def append_data(self, voyages_list):
        """ Adds new data to data file """
        data_list = []
        for voyage in voyages_list:
            data_list.append(voyage.get_data_list())
        
        with open(self.data_id, "a", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def __read_data(self):
        """ Opens and reads in csv data file for reading """
        data_list = []
        file_stream = open(self.data_id, "r")
        for line in file_stream:
            line_data = line.strip().split(",")
            if data_list != []:
                if len(line_data[5]) != 0:
                    line_data[5] = [int(i) for i in line_data[5].split(";")]
                else:
                    line_data[5] = []
            data_list.append(line_data)
        file_stream.close()
        return data_list

"""
voy = Voyage_IO()
voy.get_data()

voy = Voyage_IO()
voyage = Voyage(1, 2, "2019-12-24T18:00:00", "NA", "NA", [123, 123], False, 53)
voy.append_data([voyage])
"""