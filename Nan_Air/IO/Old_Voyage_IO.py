from Nan_Air.Model_classes.Old_Voyage import Old_Voyage
import csv

class Old_Voyage_IO():
    data_id = "Nan_Air/Data/data_old_voyage.csv"

    def get_data(self):
        """ Returns list of Voyage instances from file """
        data_list = self.__read_data()
        voyages_list = []

        for line in data_list:
            if line != data_list[0]:
                old_voyage = Old_Voyage(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[9], line[8])
                voyages_list.append(old_voyage)
        return voyages_list

    def write_data(self, old_voyages_list):
        """ Overwrites all data in file with new data """
        data_list = [["Plane", "Destination", "Departure 1 time", "Arrival 1 time", "Departure 2 time", "Arrival 2 time", "Flight 1", "Flight 2", "Employee id list"]]
        for old_voyage in old_voyages_list:
            data_list.append(old_voyage.get_data_list())
        
        with open(self.data_id, "w", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def append_data(self, old_voyages_list):
        """ Adds new data to data file """
        data_list = []
        for old_voyage in old_voyages_list:
            data_list.append(old_voyage.get_data_list())
        
        with open(self.data_id, "a", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)
        f.close()

    def __read_data(self):
        """ Opens and reads in csv data file for reading """
        data_list = []
        file_stream = open(self.data_id, "r", encoding="utf-8")
        for line in file_stream:
            line_data = line.strip().split(",")
            line_data[7] = line_data[7].split(";")
            data_list.append(line_data)
        file_stream.close()
        return data_list

"""
voy = Old_Voyage_IO()
#reset = Old_Voyage("Plane", "Destination", "Departure 1 time", "Arrival 1 time", "Departure 2 time", "Arrival 2 time", "Flight 1", "Flight 2", ["Employee id list"])
#voy.write_data([reset])
#test = Old_Voyage("TF-NAB", "BESS", "14:00:00", "16:00:00", "17:00:00", "19:00:00", "NA0201", "NA0202", ["123: Gunni", "124: Gunna"])
#voy.append_data([test])
voy.get_data()
"""