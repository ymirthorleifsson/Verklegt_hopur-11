from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Destination import Destination
from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.Model_classes.Old_Voyage import Old_Voyage
from Nan_Air.Model_classes.Flight import Flight



from Nan_Air.IO_API import IO_API
from Nan_Air.LL.Aircraft_LL import Aircraft_LL
from Nan_Air.LL.Employee_LL import Employee_LL
from Nan_Air.LL.Destination_LL import Destination_LL
from Nan_Air.LL.Voyage_LL import Voyage_LL
from Nan_Air.LL.Flight_LL import Flight_LL
from Nan_Air.LL.Old_Voyage_LL import Old_Voyage_LL

import datetime
class LL_API():
    # Object resolve
    # NEEDS TO BE IMPLEMENTED
    def get_working_list_info(employee, time):
        return Employee_LL.get_working_list_info(employee, time)

    def get_list_info(item):
        if isinstance(item, Employee):
            return Employee_LL.get_list_info(item)
        elif isinstance(item, Aircraft):
            return Aircraft_LL.get_list_info(item)
        elif isinstance(item, Destination):
            return Destination_LL.get_list_info(item)
        #-------------------------------------------
        elif isinstance(item, Voyage):
            return Voyage_LL.get_list_info(item)
        elif isinstance(item, Old_Voyage):
            return Old_Voyage_LL.get_list_info(item)
        #----------------------------------
        elif type(item) == type(Flight()):
            return Flight_LL.get_list_info(item)
        else:
            1/0

    def get_info(item):
        if isinstance(item, Employee):
            return Employee_LL.get_info(item)
        elif isinstance(item, Aircraft):
            return Aircraft_LL.get_info(item)
        elif isinstance(item, Destination):
            return Destination_LL.get_info(item)
        # -------------------------------------------
        elif isinstance(item, Voyage):
            return Voyage_LL.get_info(item)
        elif isinstance(item, Old_Voyage):
            return Old_Voyage_LL.get_info(item)
        #-------------------------------
        elif type(item) == type(Flight()):
            return Flight_LL.get_info(item)
        else:
            1/0 # BOMBA

    def get_edit_info(item):
        if isinstance(item, Employee):
            return Employee_LL.get_edit_info(item)
        elif isinstance(item, Destination):
            return Destination_LL.get_edit_info(item)
        # -------------------------------------------
        elif isinstance(item, Voyage):
            return Voyage_LL.get_edit_info(item)
        #-------------------------
        elif type(item) == type(Flight()):
            return Flight_LL.get_edit_info(item)
        else:
            1/0

    #Employee LL
    def get_number_of_employees(search, list_filter, time):
        return Employee_LL.get_number_of_employees(search, list_filter, time)

    def get_employee_page(page, search, employees_per_page, list_filter, time):
        return Employee_LL.get_employee_page(page, search, employees_per_page, list_filter, time)

    def store_employee(employee):
        Employee_LL.store_employee(employee)

    def change_employee(employee_ID, new_employee_info):
        Employee_LL.change_employee(employee_ID, new_employee_info)

    # Destination LL
    def get_number_of_destinations(search):
        return Destination_LL.get_number_of_destinations(search)

    def get_destination_page(search, page, items_per_page):
        return Destination_LL.get_destination_page(search, page, items_per_page)

    def store_destination(destination):
        Destination_LL.store_destination(destination)

    def change_destination(destinatino_id, destination):
        Destination_LL.change_destination(destinatino_id, destination)

    #Aircraft LL

    def get_number_of_aircraft(search):
        return Aircraft_LL.get_number_of_aircraft(search)

    def get_aircraft_page(search, page, employees_per_page):
        return Aircraft_LL.get_aircraft_page(search, page, employees_per_page)

    def store_aircraft(aircraft):
        Aircraft_LL.store_aircraft(aircraft)
    
    def set_filter_time(time_string):
        Aircraft_LL.TIME_TO_FIND = time_string
    # VOYAGE LL ree

    def get_number_of_voyages(search, manned_filter):
        return Voyage_LL.get_number_of_voyages(search, manned_filter)

    def get_voyage_page(search, page, items_per_page, search_filter):
        return Voyage_LL.get_voyage_page(search, search_filter, page, items_per_page)

    def store_new_voyage(destination_id, departure_time):
        Voyage_LL.store_new_voyage(destination_id, departure_time)

    def change_voyage(voyage_id, new_info):
        Voyage_LL.change_voyage(voyage_id, new_info)
    #Flight LL

    def get_number_of_flights(search):
        return Flight_LL.get_number_of_flights(search)

    def get_flight_page(page, items_per_page, search):
        return Flight_LL.get_flight_page(page, items_per_page, search)
        #-----------------------------------


    def get_object_page(object_id, search, page, objects_per_page):
        filtered_object_list = LL_API.get_object_list(object_id, search)
        return get_page_from_list(filtered_object_list, page, objects_per_page)

    def get_object_list(object_id, search):
        object_list = LL_API.get_full_object_list(object_id)

        if search != None:
            search_filtered_object_list = []
            for item in object_list:
                if search in str(item).lower():
                    search_filtered_object_list += [item]
        else:
            search_filtered_object_list = [] + object_list

        sorted_object_list = [] + sorted(search_filtered_object_list, key=lambda x: str(x).lower())
        return sorted_object_list

    def get_number_of_objects(object_id, search):
        object_list = LL_API.get_object_list(object_id, search)
        return len(object_list)

    def get_full_object_list(object_id):
        return IO_API.get_data(object_id)

    def write_aircraft_list(aircraft_list):
        IO_API.write_data(Aircraft_LL.AIRCRAFT_ID, aircraft_list)

    def append_aircraft_list(aircraft_list):
        IO_API.append_data(Aircraft_LL.AIRCRAFT_ID, aircraft_list)

    #-----------------
    def get_available_aircraft(time):
        aircraft = IO_API.get_data("aircraft")
        return aircraft

    def get_available_pilots(time, aircraft):
        available_employees = Employee_LL.get_available_employees(time)

        available_pilots = []
        for employee in available_employees:
            if employee.get_job() == "pilot":
                available_pilots += [employee]

        licenced_pilots = []
        for pilot in available_pilots:
            if pilot.get_licence() == aircraft.get_model().split("-")[0]:
                licenced_pilots += [pilot]

        return licenced_pilots

    def get_available_employees(time):
        available_employees = Employee_LL.get_available_employees(time)
        return available_employees

    def get_available_destinations():
        all_destinations = IO_API.get_data("destination")
        return all_destinations

    def get_work_week(item, time):
        work_week_days = []
        for i in range(7):
            work_week_days += [time+datetime.timedelta(i)]
        voyages = IO_API.get_data("voyage")

        info_list = []
        for voyage in voyages:
            for workday in Voyage_LL.get_voyage_working_days(voyage):
                for i in range(len(work_week_days)):
                    if workday == work_week_days[i]:
                        if item.get_id() in voyage.get_employee_ids():
                            destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
                            work_week_days[i] = (workday, destination)


        for i in range(len(work_week_days)):
            if not isinstance(work_week_days[i], datetime.datetime):
                work_week_days[i] = (work_week_days[i][0], work_week_days[i][1].get_country())
            else:
                work_week_days[i] = (work_week_days[i], None)
        return work_week_days

    def get_voyages_time(search, list_filter, time_filter):
        voyages = IO_API.get_data("voyage")
        search_filtered_voyage_list = []
        if search is not None:
            for v in voyages:
                if search in LL_API.get_list_info(v):
                    search_filtered_voyage_list += [v]
        else:
            search_filtered_voyage_list = voyages


        search_filter = list_filter
        if search_filter == "all":
            search_filter_filtered_voyage_list = search_filtered_voyage_list + Old_Voyage_LL.get_old_voyages_list(
                search)
        elif search_filter == "not finished":
            search_filter_filtered_voyage_list = search_filtered_voyage_list
        elif search_filter == "finished":
            search_filter_filtered_voyage_list = Old_Voyage_LL.get_old_voyages_list(search)
        else:
            search_filter_filtered_voyage_list = []

        time_filtered_voyage_list = []
        if time_filter is not None:
            for voyage in search_filter_filtered_voyage_list:
                if time_filter[0] <= voyage.get_departure_time() <= time_filter[1]:
                    time_filtered_voyage_list += [voyage]
        else:
            time_filtered_voyage_list = search_filter_filtered_voyage_list


        return time_filtered_voyage_list




    def get_number_of_voyage_page_time(search, list_filter, time_filter):
        voyages = LL_API.get_voyages_time(search,list_filter,time_filter)
        return  len(voyages)

    def get_voyage_page_time(search, page, ITEMS_PER_PAGE, list_filter, time_filter):
        voyages = LL_API.get_voyages_time(search, list_filter, time_filter)
        return voyages[(page-1)*ITEMS_PER_PAGE:page*ITEMS_PER_PAGE]

