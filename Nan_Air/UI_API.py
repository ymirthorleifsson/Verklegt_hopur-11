import os
import math
from Nan_Air.LL_API import LL_API
from Nan_Air.Model_classes.Button import Button
from Nan_Air.WindowV3.Render import Render
'''
from Nan_Air.UI.Aircraft_UI import Aircraft_UI
from Nan_Air.UI.Employee_UI import Employee_UI
from Nan_Air.UI.Voyage_UI import Voyage_UI
from Nan_Air.UI.Destination_UI import Destination_UI
from Nan_Air.UI.Flight_UI import Flight_UI
'''

from Nan_Air.UI.Object_Branch_UI import Object_Branch

from Nan_Air.WindowV3.List_Window.List_Window import List_Window
from Nan_Air.WindowV3.Info_Window.Info_Window import Info_Window
from Nan_Air.WindowV3.Edit_Window.Edit_Window import Edit_Window
from Nan_Air.WindowV3.Add_Window.Add_Window import Add_Window
from Nan_Air.LL_API import LL_API

from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Destination import Destination

from Nan_Air.UI.Flight_UI import Flight_UI
from Nan_Air.UI.Voyage_UI import Voyage_UI
from Nan_Air.UI.Employee_UI import Employee_UI
from Nan_Air.UI.Aircraft_UI import Aircraft_UI
from Nan_Air.UI.Destination_UI import Destination_UI
import os


INDEX_OF_NAME_IN_EMPLOYEE = 0
WIDTH = 120
BORDER = "#"

class UI_API():

    #Main_Menu ========================
    def main_menu():
        buttons = [Button("Employees", "e", Employee_UI.run),
                   Button("Aircraft", "a", Aircraft_UI.run),
                   Button("Voyages", "v", Voyage_UI.run),
                   #Button("Flights", "f", Flight_UI.run),
                   Button("Destination", "d", Destination_UI.run)]

        def display():
            Render.clear()
            Render.header("Main Menu", right="|Q| Quit")
            Render.menu([]+buttons)
            Render.the_border()

        def get_input():
            return Render.the_input()

        def main():
            nonlocal buttons
            while True:
                display()
                user_input = get_input()
                if user_input == "q":
                    return 0
                for button in buttons:
                    if user_input.lower() == button.trigger_key:
                        button.mapping_func()
        main()