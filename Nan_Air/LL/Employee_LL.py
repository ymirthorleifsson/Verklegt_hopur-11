from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Employee import Employee
from Nan_Air.LL.Voyage_LL import Voyage_LL

class Employee_LL():

    def __get_employee_list(search, filter, time):
        '''Private method which gets the employee list according to the filter and search.'''


        employee_list = IO_API.get_data("employee")

        # Checks the filter
        filtered_employee_list = []
        if filter == "pilot" or filter == "cabin_crew":
            for employee in employee_list:
                if employee.get_job() == filter:
                    filtered_employee_list += [employee]

        elif filter == "all":
            filtered_employee_list = employee_list

        else:
            all_voyages = IO_API.get_data("voyage")
            same_day_voyages = []
            for voyage in all_voyages:
                for work_day in Voyage_LL.get_voyage_working_days(voyage):
                    if work_day.year == time.year and work_day.month == time.month and work_day.day == time.day:
                        same_day_voyages += [voyage]

            if filter == "working":
                filtered_employee_list = []
                for voyage in same_day_voyages:
                    for ssn in voyage.get_employee_ids():
                        filtered_employee_list += [Voyage_LL.get_employee_by_id(ssn)]

            elif filter == "not_working":
                working = []
                for voyage in same_day_voyages:
                    for ssn in voyage.get_employee_ids():
                        working += [Voyage_LL.get_employee_by_id(ssn)]


                filtered_employee_list = list(set(employee_list)-set(working))


        employee_list = filtered_employee_list
        # Checks the search
        if search != None:
            search_filtered_employee_list = []
            for employee in employee_list:
                if search.lower() in Employee_LL.get_list_info(employee).lower():
                    search_filtered_employee_list.append(employee)
        else:
            search_filtered_employee_list = employee_list

        return search_filtered_employee_list


    def get_working_employees(time):
        all_voyages = IO_API.get_data("voyage")
        same_day_voyages = []
        for voyage in all_voyages:
            for work_day in Voyage_LL.get_voyage_working_days(voyage):
                if work_day.year == time.year and work_day.month == time.month and work_day.day == time.day:
                    same_day_voyages += [voyage]

        working_employee_list = []
        for voyage in same_day_voyages:
            for ssn in voyage.get_employee_ids():
                working_employee_list += [Voyage_LL.get_employee_by_id(ssn)]
        return working_employee_list

    def get_available_employees(time):
        all_voyages = IO_API.get_data("voyage")
        same_day_voyages = []
        for voyage in all_voyages:
            for work_day in Voyage_LL.get_voyage_working_days(voyage):
                if work_day.year == time.year and work_day.month == time.month and work_day.day == time.day:
                    same_day_voyages += [voyage]

        available_employee_list = IO_API.get_data("employee")
        for voyage in same_day_voyages:
            for ssn in voyage.get_employee_ids():
                for employee in available_employee_list:
                    if employee.get_id() == ssn:
                        available_employee_list.remove(employee)

        return available_employee_list

    def get_employee_page(search, page, items_per_page, filter, time):
        '''Gets a specific page of the employee list.'''

        employee_list = Employee_LL.__get_employee_list(search, filter, time)
        sorted_employee_list = []
        sorted_employee_list += sorted(employee_list, key=lambda x: x.get_name().lower())
        
        return sorted_employee_list[(page - 1) * items_per_page:page * items_per_page]
        
    def get_number_of_employees(search, filter, time):
        '''Gets the total number of employees'''
        employee_list = Employee_LL.__get_employee_list(search, filter, time)
        return len(employee_list)

    def store_employee(employee_info):
        new_employee = Employee(employee_info[0],
                                employee_info[1],
                                employee_info[2],
                                employee_info[3],
                                employee_info[4],
                                employee_info[5],
                                employee_info[6])
        '''Stores a new employee.'''

        IO_API.append_data("employee", [new_employee])

    def change_employee(employee_ID, new_employee_info):
        '''Stores the changes of an employee.'''
        all_employees = IO_API.get_data("employee")

        for employee in all_employees:
            if employee.get_id() == employee_ID:
                employee.set_info(new_employee_info)

        IO_API.write_data("employee", all_employees)


# New stuff ==============================================================

    def get_list_info(employee):
        # Used by list window and select window
        return "{}#{}#{}#{}".format(employee.name, employee.job, employee.rank, employee.email)


    def __get_destination_of_voyage(employee, voyages, time):
        for voyage in voyages:
            for day in Voyage_LL.get_voyage_working_days(voyage):
                if time.year == day.year and time.month == day.month and time.day == day.day:
                    for employee_id in voyage.get_employee_ids():
                        if employee_id == employee.get_id():
                            return Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        1/0 #did not find destination

    def get_working_list_info(employee, time):
        # Used by list window and select window
        all_voyages = IO_API.get_data("voyage")
        destination = Employee_LL.__get_destination_of_voyage(employee,all_voyages, time)
        return "{}#{}#{}#{}#{}".format(employee.name, employee.job, employee.rank, employee.email, destination.get_airport())

    def get_info(employee):
        # Used by info window
            return_info_list = [
                ["NAME", employee.name],
                ["SSN", str(employee.ssn)],
                ["JOB", employee.job],
                ["ADDRESS", employee.address],
                ["MOBILE NR", employee.mobile_nr],
                ["EMAIL", employee.email],
                ["RANK", employee.rank]
            ]
            return return_info_list

    def get_rank(self):
        return self.rank

    def get_edit_info(employee):
        """Returns a list with detailed informations about the employee"""
        # Used by edit window
        return_info_list = [
            ["NAME", employee.name, False],
            ["SSN", str(employee.ssn), False],
            ["JOB", employee.job, True],
            ["ADDRESS", employee.address, True],
            ["MOBILE NR", employee.mobile_nr, True],
            ["EMAIL", employee.email, True],
            ["RANK", employee.rank, True]
        ]
        return return_info_list