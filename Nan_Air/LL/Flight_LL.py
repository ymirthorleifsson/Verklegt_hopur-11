from Nan_Air.Model_classes.Flight import Flight
from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.Model_classes.Old_Voyage import Old_Voyage
from Nan_Air.Function_classes.Date_parser import Date_parser

from Nan_Air.LL.Old_Voyage_LL import Old_Voyage_LL
from Nan_Air.LL.Voyage_LL import Voyage_LL


class Flight_LL():
    
    def get_flights_list(search):
        all_voyages_list = []
        old_voyages = Old_Voyage_LL.get_old_voyages_list(search)
        voyages = Voyage_LL.get_voyage_list(search, None)
        for voyage in voyages:
            all_voyages_list.append(Voyage_LL.create_old_voyage(voyage))
        return Flight_LL.get_flight_from_old_voyages(old_voyages)

    def get_number_of_flights(search):
        return len(Flight_LL.get_flights_list(search))

    def get_flight_page(page, items_per_page, search):
        flights = Flight_LL.get_flights_list(search)
        return flights[(page-1)*items_per_page:page*items_per_page]



    def get_flight_from_old_voyages(old_voyages_list):
        """ Takes in a list of old voyages objects and returns a list of flights """

        flights_list = []
        for old_voyage in old_voyages_list:
            # Flight 1
            flight_1 = Flight(old_voyage.flight_1_number, old_voyage.plane_str, "BIRK", old_voyage.destination_str, \
                 Date_parser.date_parser(old_voyage.departure_1_time), Date_parser.date_parser(old_voyage.arrival_1_time), old_voyage.employees_string_list)
            # Flight 2
            flight_2 = Flight(old_voyage.flight_2_number, old_voyage.plane_str, old_voyage.destination_str, "BIRK", \
                Date_parser.date_parser(old_voyage.departure_2_time), Date_parser.date_parser(old_voyage.arrival_2_time), old_voyage.employees_string_list)
            flights_list.append((flight_1, flight_2))

        return flights_list

    def get_info(flight):
        """ Returns an info list of all information for detailed info """
        pilots, cabin_crew = Old_Voyage_LL.get_pilot_cabin(flight.employees_string_list)
        detailed_info = [
                ["FLIGHT 1", "", 1],
                ["FLIGHT NUMBER", flight.flight_number_str, 2],
                ["DESTINATION", flight.destination_str, 2],
                ["DEPARTURE", flight.departure_str, 2],
                ["DEPARTURE TIME", flight.departure_time, 2],
                ["ARRIVAL TIME", flight.arrival_time, 2],
                ["AIRCRAFT NAME", flight.aircraft_str, 1],
            ]
        # Add pilots to the detailed info list
        detailed_info += [["PILOTS", "", 1]]
        for pilot in pilots:
            name, rank = pilot.split(":")
            detailed_info.append([rank, name, 2])
        detailed_info += [["CABIN CREW:", "", 1]]
        # Add cabin crew to the detailed info list
        for cc in cabin_crew:
            name, rank = cc.split(":")
            detailed_info.append([rank, name, 2])
        return detailed_info
    
    def get_list_info(flight):
        departure, arrival = flight.get_flight_times_str()
        return "{}#{}#{}#{}#{}".format(flight.departure_str, flight.destination_str, departure, arrival, flight.get_status())


    
    