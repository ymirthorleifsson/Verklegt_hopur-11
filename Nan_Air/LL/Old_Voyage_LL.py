from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Old_Voyage import Old_Voyage

class Old_Voyage_LL():
    def get_old_voyages_list(search):
        """ Gets a list of voyages from file and returns it. Applies search or search filter if they are not None """
        all_old_voyages_list = IO_API.get_data("old_voyage")

        # Search
        if search != None:
            search_filtered_old_voyages_list = []
            for old_voyage in all_old_voyages_list:
                if search.lower() in Old_Voyage_LL.get_list_info(old_voyage).lower(): # Check if search is in voyage string
                    search_filtered_old_voyages_list.append(old_voyage)
        # If no search is applied
        else:
            search_filtered_old_voyages_list = all_old_voyages_list

        return search_filtered_old_voyages_list
    
    def get_list_info(old_voyage):
        """ Returns a string with old voyage information for old voyage to display """
        pilots, cabin_crew = Old_Voyage_LL.get_pilot_cabin(old_voyage.employees_string_list)

        return "{}#{}#{}#{}#{}#{}#{}".format(
            old_voyage.destination_str,
            old_voyage.departure_1_time,
            old_voyage.plane_str,
            len(pilots),
            len(cabin_crew),
            "Manned",
            "Finished")
    
    def get_number_of_old_voyages(search):
        """ Returns the length of the old voyage list """

        return len(Old_Voyage_LL.get_old_voyages_list(search))
    
    def get_pilot_cabin(employees_string_list):
        """ Takes in a employee string list and returns a pilot and a cabin crew list """

        pilots = []
        cabin_crew = []
        for employee in employees_string_list:
            if "captain" in employee:
                pilots.append(employee)
            elif "copilot" in employee:
                pilots.append(employee)
            elif "flight_service_manager" in employee:
                cabin_crew.append(employee)
            elif "flight_attendant" in employee:
                cabin_crew.append(employee)
        return pilots, cabin_crew


    def get_info(old_voyage):
        """ Returns an info list for detailed info window to display """

        pilots, cabin_crew = Old_Voyage_LL.get_pilot_cabin(old_voyage.employees_string_list)
        detailed_info = [
                ["FLIGHT 1", "", 1],
                ["FLIGHT NUMBER", old_voyage.flight_1_number, 2],
                ["DESTINATION", old_voyage.destination_str, 2],
                ["DEPARTURE", "BIRK", 2],
                ["DEPARTURE TIME", old_voyage.departure_1_time, 2],
                ["ARRIVAL TIME", old_voyage.arrival_1_time, 2],
                ["FLIGHT 2", "", 1],
                ["FLIGHT NUMBER", old_voyage.flight_2_number, 2],
                ["DESTINATION", "BIRK", 2],
                ["DEPARTURE", old_voyage.destination_str, 2],
                ["DEPARTURE TIME", old_voyage.departure_2_time, 2],
                ["ARRIVAL TIME", old_voyage.arrival_2_time, 2],
                ["AIRCRAFT NAME", old_voyage.airplane_str, 1],
            ]
        # Add pilots to the detailed info list
        detailed_info += [["PILOTS", "", 1]]
        for pilot in pilots:
            name, rank = pilot.split(":")
            detailed_info.append([rank, name, 2])
        detailed_info += [["CABIN CREW:", "", 1]]
        # Add cabin crew to the detailed info list
        for cc in cabin_crew:
            name, rank = cc.split(":")
            detailed_info.append([rank, name, 2])
        return detailed_info
    
    def store_old_voyage(old_voyage):
        """ Take in an a Old Voyage object, gives it a new id and writes it to file """
        old_voyage.set_id(Old_Voyage_LL.get_new_id(old_voyage))
        IO_API.append_data("old_voyage", [old_voyage])

    def get_new_id(old_voyage):
        old_voyages_list = IO_API.get_data("old_voyage")
        max_id = 0
        for old in old_voyages_list:
            if old.get_id() > max_id:
                max_id = old.get_id()
        return max_id + 1