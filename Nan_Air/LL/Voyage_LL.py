from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.Model_classes.Destination import Destination
from Nan_Air.Function_classes.Date_parser import Date_parser
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Old_Voyage import Old_Voyage
from Nan_Air.LL.Old_Voyage_LL import Old_Voyage_LL
import datetime


INDEX_OF_CREW_STATUS = 5

class Voyage_LL():
    
    def get_voyage_list(search, search_filter):
        return Voyage_LL.__get_voyage_list(search, search_filter)


    def get_voyages_on_day(day):
        voyages = IO_API.get_data("voyage")
        for voyage in voyages:
            pass
        #REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE


    # TIME LOGIC

    def get_voyage_working_days(voyage):
        voyage_departure_time = Voyage_LL.__get_departure_day_of_voyage(voyage)
        voyage_arrival_time = Voyage_LL.__get_arrival_day_of_voyage(voyage)
        # if voyage uses two days
        if voyage_arrival_time != voyage_departure_time:
            return [voyage_departure_time, voyage_arrival_time]
        else:
            return [voyage_departure_time]


    def get_voyage_page(search, search_filter, page, items_per_page):
        voyage_list = Voyage_LL.get_voyage_list(search, search_filter)
        return voyage_list[(page - 1)*items_per_page:page*items_per_page]

    def get_number_of_voyages(search, search_filter):
        voyage_info_list = Voyage_LL.get_voyage_list(search, search_filter)
        return len(voyage_info_list)


    def __get_departure_day_of_voyage(voyage):
        departure_day = voyage.get_departure_time()
        departure_day = datetime.datetime(departure_day.year,
                                              departure_day.month,
                                              departure_day.day)
        return departure_day

    def __get_arrival_day_of_voyage(voyage):
        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        arrival_day = Date_parser.get_arrival_times(str(voyage.get_departure_time()), str(destination.get_flight_time()))[3]
        arrival_day = datetime.datetime(arrival_day.year,
                                              arrival_day.month,
                                              arrival_day.day)
        return arrival_day
    
    def get_all_times(voyage):
        """ Returns a tuple of 4 values. Departure times and arrival times of both flights from voyage """
        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        return Date_parser.get_arrival_times(voyage.get_departure_time_str(), destination.get_flight_time())
        

    # WRITING IO LAYER

    def store_new_voyage(destination_id, departure_time):

        flight_number_1,\
        flight_number_2 = Voyage_LL.__get_voyage_flight_numbers(destination_id,
                                                            departure_time)
        new_voyage = Voyage(None,
                            destination_id,
                            str(departure_time),
                            flight_number_1,
                            flight_number_2,
                            [],
                            False,
                            Voyage_LL.get_new_voyage_id())
        IO_API.append_data("voyage", [new_voyage])

    def change_voyage(voyage_ID, new_info):
        full_voyage_list = IO_API.get_data("voyage")

        for voyage in full_voyage_list:
            if voyage.get_id() == voyage_ID:
                voyage.set_info(new_info)
                break

        IO_API.write_data("voyage", full_voyage_list)

    def get_new_voyage_id():
        all_voyages = IO_API.get_data("voyage")
        max_id = 0
        for voyage in all_voyages:
            if max_id < voyage.get_id():
                max_id = voyage.get_id()
        return max_id + 1

    def __get_voyage_flight_numbers(destination_id, departure_time):
        all_voyages = IO_API.get_data("voyage")
        departure_day = datetime.datetime(departure_time.year,
                                          departure_time.month,
                                          departure_time.day)
        number_of_voyages_with_same_departure_day_and_destination = 0
        for voyage in all_voyages:
            if Voyage_LL.__get_departure_day_of_voyage(voyage) == departure_day \
                    and voyage.get_destination_id() == destination_id:
                number_of_voyages_with_same_departure_day_and_destination += 1

        destination_id_str = str(destination_id)
        if len(destination_id_str) == 1:
            destination_id_str = "0" + destination_id_str

        return "NA{}{}".format(destination_id_str, number_of_voyages_with_same_departure_day_and_destination * 2), \
               "NA{}{}".format(destination_id_str, number_of_voyages_with_same_departure_day_and_destination * 2 + 1)

    # RESOLVE POINTERS

    def get_destination_by_id(destination_id):
        all_destinations = IO_API.get_data("destination")
        for destination in all_destinations:
            if destination.get_id() == destination_id:
                return destination
        return "ERROR NO DESTINATION WITH THAT ID"

    def get_aircraft_by_id(aircraft_id):
        all_aircraft = IO_API.get_data("aircraft")
        for aircraft in all_aircraft:
            if aircraft.get_id() == aircraft_id:
                return aircraft
        return "ERROR NO AIRCRAFT WITH THAT ID"

    def get_employee_by_id(employee_id):
        all_employees = IO_API.get_data("employee")
        for employee in all_employees:
            if employee.get_id() == employee_id:
                return employee
        return "ERROR NO EMPLOYEE WITH THAT ID"
    
    def get_aircraft_str(voyage):
        aircraft = Voyage_LL.get_aircraft_by_id(voyage.get_aircraft_id())
        return aircraft.get_name()
    
    def get_destination_str(voyage):
        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        return destination.get_code_str

    # VOYAGE INFO FUNCTIONS

    def get_list_info(voyage):
        aircraft = Voyage_LL.get_aircraft_by_id(voyage.get_aircraft_id())
        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        employees = []
        for employee_id in voyage.get_employee_ids():
            employees += [Voyage_LL.get_employee_by_id(employee_id)]

        number_of_pilots = 0
        number_of_cabin_crew = 0
        for employee in employees:
            if employee.get_job() == "pilot":
                number_of_pilots += 1
            elif employee.get_job() == "cabin_crew":
                number_of_cabin_crew += 1

        if voyage.get_aircraft_id() is not None:
            aircraft_name = aircraft.get_name()
        else:
            aircraft_name = "NO AIRCRAFT"
        destination_code = destination.get_code()

        return "{}#{}#{}#{}#{}#{}#{}".format(destination_code,
                         voyage.get_departure_time(),
                         aircraft_name,
                         number_of_pilots,
                         number_of_cabin_crew,
                         voyage.get_crew_status(),
                         "unfinished")

    def get_info(voyage):
        if voyage.get_aircraft_id() is not None:
            aircraft = Voyage_LL.get_aircraft_by_id(voyage.get_aircraft_id())
        else:
            aircraft = None

        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        employees = []
        for employee_id in voyage.get_employee_ids():
            employees += [Voyage_LL.get_employee_by_id(employee_id)]

        pilots = []
        cabin_crew = []
        for employee in employees:
            if employee.get_job() == "pilot":
                pilots += [employee]
            elif employee.get_job() == "cabin_crew":
                cabin_crew += [employee]

        departure_time = voyage.get_departure_time()
        if aircraft is not None:
            aircraft_name = aircraft.get_name()
            aircraft_model = aircraft.get_model()
        else:
            aircraft_name = "None"
            aircraft_model = "None"
        destination_airport = destination.get_airport()
        destination_code = destination.get_code()
        destination_flight_time = destination.get_flight_time()
        departure_time, arrival_time, departure_2_time, arrival_2_time = Date_parser.get_arrival_times(str(departure_time), destination_flight_time)
        destination_dist_from_birk = destination.get_dist_from_birk() + " km"


        detailed_info = \
            [
                ["FLIGHT 1", None, 1],
                ["FLIGHT NUMBER", voyage.flight1_number, 2],
                ["DESTINATION", "{} ({})".format(destination_code, destination_airport), 2],
                ["DEPARTURE", "BIRK (Reykjavik)", 2],
                ["DEPARTURE TIME", str(departure_time), 2],
                ["ARRIVAL TIME", str(arrival_time), 2],
                ["DISTANCE", destination_dist_from_birk, 2],
                ["FLIGHT 2", None, 1],
                ["FLIGHT NUMBER", voyage.flight2_number, 2],
                ["DESTINATION", "BIRK (Reykjavik)", 2],
                ["DEPARTURE", "{} ({})".format(destination_code, destination_airport), 2],
                ["DEPARTURE TIME", str(departure_2_time), 2],
                ["ARRIVAL TIME", str(arrival_2_time), 2],
                ["DISTANCE", destination_dist_from_birk, 2],
                ["AIRCRAFT NAME", aircraft_name, 1],
                ["AIRCRAFT MODEL", aircraft_model, 1],
            ]
        detailed_info += [["PILOTS", None, 1]]
        for pilot in pilots:
            detailed_info += [[pilot.get_rank(), pilot.get_name(), 2]]
        detailed_info += [["CABIN CREW:", None, 1]]
        for cc in cabin_crew:
            detailed_info += [[cc.get_rank(), cc.get_name(), 2]]
        return detailed_info

    def get_edit_info(voyage):
        if voyage.get_aircraft_id() is None:
            voyage_aircraft = None
        else:
            voyage_aircraft = Voyage_LL.get_aircraft_by_id(voyage.get_aircraft_id())

        voyage_captain = None
        voyage_copilot_list = []
        voyage_flight_service_manager = None
        voyage_flight_attendant_list = []

        for employee_id in voyage.get_employee_ids():
            temp_employee = Voyage_LL.get_employee_by_id(employee_id)
            if temp_employee.get_job() == "pilot":
                if temp_employee.get_rank() == "captain":
                    voyage_captain = temp_employee
                elif temp_employee.get_rank() == "copilot":
                    voyage_copilot_list += [temp_employee]
            elif temp_employee.get_job() == "cabin_crew":
                if temp_employee.get_rank() == "flight_service_manager":
                    voyage_flight_service_manager = temp_employee
                elif temp_employee.get_rank() == "flight_attendant":
                    voyage_flight_attendant_list += [temp_employee]

        return [voyage_aircraft, voyage_captain, voyage_copilot_list, voyage_flight_service_manager, voyage_flight_attendant_list]

    def get_voyage_page(search, search_filter, page, items_per_page):
        voyage_list = Voyage_LL.__get_voyage_list(search, search_filter)
        return voyage_list[(page - 1)*items_per_page:page*items_per_page]

    def get_number_of_voyages(search, search_filter):
        voyage_info_list = Voyage_LL.__get_voyage_list(search, search_filter)
        return len(voyage_info_list)

    def __get_voyage_list(search, search_filter):
        all_voyage_list = IO_API.get_data("voyage")
        old_voyages = Voyage_LL.voyages_status_checker(all_voyage_list)
        all_voyage_list = [voyage for voyage in all_voyage_list if voyage not in old_voyages]
        IO_API.write_data("voyage", all_voyage_list)
        # Search filter
        if search != None:
            search_filtered_voyage_list = []
            for voyage in all_voyage_list:
                if search.lower() in Voyage_LL.get_list_info(voyage).lower():
                    search_filtered_voyage_list += [voyage]
        else:
            search_filtered_voyage_list = all_voyage_list

        # search_filter can be ["all", "not_finished", "finished", "manned", "unmanned"]
        if search_filter == "all":
            search_filter_filtered_voyage_list = search_filtered_voyage_list + Old_Voyage_LL.get_old_voyages_list(search)
        elif search_filter == "not finished":
            search_filter_filtered_voyage_list = search_filtered_voyage_list
        elif search_filter == "finished":
            search_filter_filtered_voyage_list = Old_Voyage_LL.get_old_voyages_list(search)
        else:
            search_filter_filtered_voyage_list = []
            for voyage in search_filtered_voyage_list:
                ##print(voyage.get_crew_status(), search_filter)
                if voyage.get_crew_status() == search_filter:
                    search_filter_filtered_voyage_list += [voyage]

        return search_filter_filtered_voyage_list


    def check_if_voyage_complete(voyage):
        """ Checks if a voyage is complete or not. Returns true if it is complete and None otherwise """

        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        departure_time = voyage.get_departure_time().isoformat(timespec="seconds")
        dep, arr, dep_2, arr_2 = Date_parser.get_arrival_times(departure_time, destination.get_flight_time())
        if arr_2 < datetime.datetime.now():
            return True
        return False

    def voyages_status_checker(voyages_list):
        """ Takes in a voyage list and checks all voyages if they are complete or not. If complete store new old voyage in file 
            and append voyage to return _ist Returns return_list"""
        return_list = []
        for voyage in voyages_list:
            if Voyage_LL.check_if_voyage_complete(voyage) == True:
                old_voyage = Voyage_LL.create_old_voyage(voyage)
                Old_Voyage_LL.store_old_voyage(old_voyage)
                return_list.append(voyage)

        return return_list

    
    def create_old_voyage(voyage):
        """ Takes in a Voayge object and returns an old voyage object """

        # Variables for Old_Voyage object
        destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
        departure_1_time, arrival_1_time, departure_2_time, arrival_2_time = \
            Date_parser.get_arrival_times(voyage.get_departure_time().isoformat(timespec="seconds"), destination.get_flight_time())
        # Check if voyage has an airplane assigned    
        if voyage.get_aircraft_id() != None:
            aircraft = Voyage_LL.get_aircraft_by_id(voyage.get_aircraft_id())
            aircraft_str = aircraft.get_name()
        else:
            aircraft_str = "None"
        destination_str = destination.code
        unique_id = voyage.get_id()
        flight_1_number, flight_2_number = voyage.flight1_number, voyage.flight2_number
        employees_list = Voyage_LL.get_employees_list(voyage.get_employee_ids())

        # Create Old_Voyage object
        old_voyage = Old_Voyage(aircraft_str, destination_str, \
            departure_1_time.isoformat(), arrival_1_time.isoformat(), \
            departure_2_time.isoformat(), arrival_2_time.isoformat(), flight_1_number, flight_2_number, \
            unique_id, employees_list)

        return old_voyage

    def get_employees_list(employee_id_list):
        """ Takes in a list of employee ids and returns a list of "name:rank" pairs """

        # Create list of Employee objects from employee ids
        employees_list = []
        for employee_id in employee_id_list:
            employees_list.append(Voyage_LL.get_employee_by_id(employee_id))
            
        # Create a list of strings with name:rank as the values
        employees_return_list = []
        for employee in employees_list:
            name = employee.get_name()
            rank = employee.get_rank()
            employees_return_list.append(name + ":" + rank)

        return employees_return_list
