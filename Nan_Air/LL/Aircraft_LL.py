from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Aircraft import Aircraft
from Nan_Air.Model_classes.Voyage import Voyage
from Nan_Air.LL.Voyage_LL import Voyage_LL
from Nan_Air.Function_classes.Date_parser import Date_parser
import datetime


class Aircraft_LL():
    TIME_TO_FIND = str(datetime.datetime.now().isoformat(timespec="seconds"))

    def __get_aircraft_list(search, pilot):
        '''Private method which gets the aircraft list according to the filter and search.'''

        aircraft_list = IO_API.get_data("aircraft")
        
        # Checks the filter (pilot)
        if pilot != None:
            for aircraft in aircraft_list:
                if pilot[5:] != aircraft.get_model():
                    aircraft_list.remove(aircraft)

        # Checks the search

        search_filtered_aircraft_list = []
        if search != None:
            for aircraft in aircraft_list:
                if search.lower() in Aircraft_LL.get_list_info(aircraft).lower():
                    search_filtered_aircraft_list += [aircraft]
        else:
            search_filtered_aircraft_list = aircraft_list
        
        return search_filtered_aircraft_list

    def get_aircraft_page(search, page, items_per_page, pilot=None):
        '''Gets a specific page of the aircraft list.'''

        aircraft_list = Aircraft_LL.__get_aircraft_list(search, pilot)
        if page == None:
            return aircraft_list

        sorted_aircraft_list = []
        sorted_aircraft_list += sorted(aircraft_list, key = lambda x: x.get_name().lower())

        return sorted_aircraft_list[(page - 1) * items_per_page:page * items_per_page]

    def get_number_of_aircraft(search, pilot=None):
        '''Gets the total number of aircrafts.'''

        aircraft_list = Aircraft_LL.__get_aircraft_list(search, pilot)

        return len(aircraft_list)

    def get_new_aircraft_id():
        aircraft_list = IO_API.get_data("aircraft")
        max_id = 0
        for aircraft in aircraft_list:
            if aircraft.get_id() > max_id:
                max_id = aircraft.get_id()
        return max_id + 1

    def store_aircraft(new_aircraft_data):
        '''Stores a new aircraft.'''
        new_aircraft = Aircraft(new_aircraft_data[0],
                                new_aircraft_data[1],
                                new_aircraft_data[2],
                                new_aircraft_data[3],
                                Aircraft_LL.get_new_aircraft_id())

        IO_API.append_data("aircraft", [new_aircraft])


# New stuff ==================================================================

    def get_voyages(aircraft_id):
        """ Takes in an aircraft id and returns a list of all the future voyages the aircraft is assigned to """

        voyages_list = IO_API.get_data("voyage")
        aircraft_voyages_list = []
        for voyage in voyages_list:
            if voyage.aircraft_id == aircraft_id:
                aircraft_voyages_list.append(voyage)
        return aircraft_voyages_list

    def get_aircraft_status(aircraft_id, time_to_find):
        """ Takes in an aircraft id and returns its status. If it is unavailable then returns time when it will be available """

        time_to_find = Date_parser.date_parser(time_to_find)
        status = "Available"
        flight_info = "N/A"
        voyages_list = Aircraft_LL.get_voyages(aircraft_id)
        for voyage in voyages_list:
            destination = Voyage_LL.get_destination_by_id(voyage.get_destination_id())
            time_to_dest_str = destination.get_flight_time()
            departure_time, arrival_time, departure_2_time, arrival_2_time = Date_parser.get_arrival_times(str(voyage.get_departure_time()), time_to_dest_str)

            if departure_time <= time_to_find <= arrival_2_time:
                status = "Available on: " + str(arrival_2_time)
                if departure_time <= time_to_find <= arrival_time:
                    flight_info = voyage.flight1_number + " to " + destination.get_airport()
                elif departure_2_time <= time_to_find <= arrival_2_time:
                    flight_info = voyage.flight2_number + " to BIRK"

        return status, flight_info

    def get_list_info(aircraft):
        status, flight_info = Aircraft_LL.get_aircraft_status(aircraft.get_id(), Aircraft_LL.TIME_TO_FIND)
        return "{}#{}#{}#{}#{}#{}".format(aircraft.name, aircraft.model, aircraft.capacity, aircraft.manufacturer, status, flight_info)

    def get_info(aircraft):
        """Returns a list with detailed informations about the employee"""
        status, flight_info = Aircraft_LL.get_aircraft_status(aircraft.get_id(), Aircraft_LL.TIME_TO_FIND)
        return_info_list = [
            ["MODEL", aircraft.model],
            ["NAME", aircraft.name],
            ["CAPACITY", aircraft.capacity],
            ["MANUFACTURER", aircraft.manufacturer],
            ["STATUS", status],
            ["FLIGHT", flight_info]
        ]
        return return_info_list

    