from Nan_Air.IO_API import IO_API
from Nan_Air.Model_classes.Destination import Destination
#from Nan_Air.Model_classes.Destination import Destination

class Destination_LL():

    def __get_destination_list(search):
        '''Private method which gets the destination list according to the filter and search.'''

        destination_list = IO_API.get_data("destination")
        search_filtered_destination_list = []
        if search != None:
            for destination in destination_list:
                if search.lower() in Destination_LL.get_list_info(destination).lower():
                    search_filtered_destination_list += [destination]
        else:
            search_filtered_destination_list = destination_list

        return search_filtered_destination_list

    def get_destination_page(search, page, items_per_page):
        '''Gets a specific page of the destination list.'''

        destination_list = Destination_LL.__get_destination_list(search)
        if page == None:
            return destination_list
        
        return destination_list[(page - 1) * items_per_page : page * items_per_page]

    def get_number_of_destinations(search):
        '''Gets the total number of destinations.'''

        destination_list = Destination_LL.__get_destination_list(search)
        return len(destination_list)

    def get_new_destination_id():
        destination_list = IO_API.get_data("destination")
        max_id = 0
        for destination in destination_list:
            if destination.get_id() > max_id:
                max_id = destination.get_id()
        return max_id + 1

    def store_destination(destination_info):
        '''Stores a new destination.'''


        new_destination = Destination(destination_info[1],
                                      destination_info[2],
                                      destination_info[3],
                                      destination_info[4],
                                      destination_info[5],
                                      destination_info[6],
                                      Destination_LL.get_new_destination_id(),
                                      destination_info[0]
                                      )

        IO_API.append_data("destination", [new_destination])

    def change_destination(destination_ID, new_destination_info):
        '''Stores the changes of a destination.'''
        
        all_destinations = IO_API.get_data("destination")

        for destination in all_destinations:
            if destination.get_id() == destination_ID:
                destination.set_info(new_destination_info)

        IO_API.write_data("destination", all_destinations)

# New stuff ==========================================================================================

    def get_list_info(destination):
        # Fallid sem ui_LL kallar i thegar hann vill upplysingar fyrir listanum
        return "{}#{}#{}#{}#{}#{}#{}".format(destination.country_str, destination.airport, destination.code, destination.flight_time, destination.dist_from_birk, destination.contact_name, destination.contact_number)

    def get_info(destination):
        """Returns a list with detailed informations about the destination"""
        return_info_list = [
            ["COUNTRY", destination.country_str],
            ["AIRPORT", destination.airport],
            ["CODE", destination.code],
            ["FLIGHT TIME", destination.flight_time],
            ["DISTANCE FROM BIRK", destination.dist_from_birk],
            ["CONTACT", destination.contact_name],
            ["CONTACT NUMBER", destination.contact_number]
        ]

        return return_info_list

    def get_edit_info(destination):
        """Returns a list with detailed informations about the destination"""
        return_info_list = [
            ["COUNTRY", destination.country_str, False],
            ["AIRPORT", destination.airport, False],
            ["CODE", destination.code, False],
            ["FLIGHT TIME (HH:MM:SS)", destination.flight_time, False],
            ["DISTANCE FROM BIRK", destination.dist_from_birk, False],
            ["CONTACT", destination.contact_name, True],
            ["CONTACT NUMBER", destination.contact_number, True]
        ]
        return return_info_list