# Verklegt_hopur-11
Git repository fyrir verklegt námskeið 1 hjá hópi 11

### 26.11.2019

##### Ástand:
  Eftir gærdaginn vorum komnir með einfaldan kröfulista og einfalda uppsetningu á skýrslunni þar sem við vorum í smá basli með að læra allt sem við lærðum ekki í greining og hönnun notendaviðmóta. Við fengum mjög vel þegna og góða hjálp frá einum í tölvunarfræði og vitum núna í hvaða röð við þurfum að gera hlutina. Eftir stuttan morgunfund ákváðum við á markmiðum fyrir daginn í dag.
  
##### Markmið:
  1. Klára alveg kröfulista og sitja hann á réttan hátt  
    - Bjartur og Guðjón
  2. Notendahópagreining  
    - Kristofer, Gummi og Ýmir  
  3. Use cases  
    - Allir

##### Staða eftir daginn:
Við náðum að klára kröfulistann, use casein og notendahópagreininguna. Það eina sem er eftir er að fara yfir nokkur use case og staðfesta rétt flæði. Við bjuggum til einfalt flow chart til að fá hugmynd um hvaða skjái við þurfum og í hvaða röð svo að Happy path í use cases séu öll í samræmi


### 27.11.2019

##### Ástand:
  Eftir stuttan morgunfund föttuðum við að við áttum eftir að búa til nytsemis- og notendaupplifunarkröfur. Við kláruðum að fara yfir use case happy pathin og ákváðum að í dag ætluðum við að vinna í klasaritum, flow-chart og reyna að byrja á stöðuritum og útlitshönnun. Eins og er þá kemst Kristofer ekki fyrr en seinna í dag svo að við ákváðum með honum að þegar að hann mætir þá mun hann detta inn í eitthvað verkefni sem vantar einhvern í.  
  
##### Markmið:
  1. Klára nytsemis- og notendaupplifunarkröfur  
    - Guðjón
  2. Klára "official" flow chart frá því sem við gerðum í gær  
    - Guðjón  
  3. Klasarit  
    - Bjartur, Gummi, Kristofer og Ýmir  
  4. Stöðurit  
    - TBD (Guðjón byrjaði)  
  5. Útlitshönnun  
    - TBD  

##### Staða eftir daginn:
  í dag náðum við að klára alveg kröfulistana, klasaritið er komið langa leið og eina sem er í raun eftir er að gera UI layerinn. Gummi og Kristofer ætla að vera lengur til að byrja að vinna í Wireframes og Happy path því þeir mættu aðeins seinna. Guðjón byrjaði á stöðuritum og kláraði eitt af þeim en skildi flow chart þar sem við erum ekki vissir um að við þurfum flow chart.


### 28.11.2019

##### Ástand:
  Eftir gærdaginn vorum við komnir vel á leið með klasaritið, byrjaðir á stöðuritum og strákarnir náðu að byrja á Wireframes sem við kusum svo hverja við ætluðum að nota. Eftir morgunfund ætlum við að reyna að klára klasaritin, stöðuritin og wireframes / happy path og þá er það eina sem er eftir er að taka stuttar notendaprófanir á wireframes til að vera vissir um að þetta sé allt í lagi hjá okkur.
  
##### Markmið:
  1. Klára klasarit:  
    - Bjartur, Guðjón, Gummi, Kristofer og Ýmir  
  2. Klára stöðurit:  
    - Guðjón  
  3. Klára wireframes og happy path:  
    - Ýmir og Kristofer  
  4. Bæta hlutum í skýrsluna:  
    - Guðjón  
    
##### Staða eftir daginn:
  Við kláruðum stöðuritin, inngangar í skýrslunni, bætt við köflum um notendaprófanir og wireframes og happy path eru komin 90%. Við settum smá pásu á klasaritin því að við fórum margir saman í að vinna í wireframes til að fá enn þá betri hugmynd um útlit forritsins. Gummi ætlaði svo að halda áfram með klasaritið í kvöld og svo klárum við það í fyrramálið.


### 29.11.2019  

##### Ástand:
  Eins og er þá er það eina sem er eftir að gera er að klára að fá inntak frá nokkrum notendum á wireframes og lagfæra ef þess þarf. Svo þarf að klára klasaritið. Guðjón mætti fyrr klukkan 8 í morgun til að undirbúa betur notendaviðtölin og að klára allt sem hægt var að klára í skýrslunni.

##### Markmið:  
  1. Klára klasarit:  
    - Bjartur, Gummi, Kristofer og Ýmir  
  2. Klára að taka notendaviðtöl:  
    - Guðjón
  3. Lagfæra wireframes ef þess þarfnast:  
    - Allir
#### Staða eftir daginn:
  Fyrsta útfærsla af hönnunarskýrslunni kláruð. Það tók nokkuð á við að klára klasaritið en það hófst á endanum en Bjartur og Guðmundur tókst mest á við það á meðan Ýmir og Kristófer lásu yfir skýrsluna og Guðjón skrifaði um notendaprófanir í skýrslunni. Næsta mánudag verður hafist handa á forritun.


### 02.12.2019

##### Ástand:
  Núna er hönnunarskýrslan búin og þá munum við byrja að forrita og útfæra forritið okkar.

##### Markmið:
  Markmið dagsins í dag er að allir horfi á git vídeóin sem voru sett á canvas. Svo förum við í fyrirlestur og munum byrja að hugsa hvernig við munum vinna að verkefninu og byrjum án efa að forrita smá.
  
#### Staða eftir daginn:
  Forritun hafin á verkefninu og allir vinna sveittum höndum. Alveg að verða komnir á það stig þannig við getum byrjað að vinna í mismunandi eiginleikum forritsins og alltaf verið með keyrsluhæfa útgáfu.
  

### 03.12.2019

##### Ástand:
  Við erum komnir vel á stað með að forrita. Bjartur og Kristofer eru að vinna í model klösunum, Ýmir og Gummi í UI og Guðjón í data layerinu.

##### Markmið:
  Halda áfram að vinna í að forrita. Guðjón ætlar að reyna að klára datalayerinn eins fljótt og auðið er. Ýmir og Gummi halda áfram að vinna í UI layerinum og Bjartur og Kristofer ætla að klára að vinna saman í model klösunum.
  
##### Staða eftir daginn:
  Komnir meira skrið í forrituninni, filter functions komin fyrir employee listann og meira. Flestir módel klasar tilbúnir.  
  
  
### 04.12.2019

##### Ástand:
  Guðjón mætti fyrr í morgun og kláraði data layer klasana. Módel klasarnir eru að mestu leiti tilbúnir, þó svo að þeir breytast af og til eftir því sem við uppfærum logic layerinn. Ýmir og Guðmundur ætla að klára meiginhlutann af UI.py á meðan Bjartur og Kristófer voru að vinna við aflúsa hitt og þetta. Einnig varð breytt skjalastrúktúrinum 

##### Markmið:
  Markmið dagsins í dag er að halda áfram að forrita og ná að setja upp gott skipurlag á skjölunum okkar (skjalastrúktúr).

##### Staða eftir daginn:
  Það gleymdist í gær að skrifa dagbókafærslu eftir daginn en í gær ákváðum við að sitja allt í folder structure og forritunin hélt vel áfram. UI er mestmegnið tilbúið, Model classes og IO layer eru tilbúin
  
  
### 05.12.2019

##### Ástand:
  Eftir gærdaginn þurfti að lagfæra aðeins folder structure því að eftir því hvaða tölvu við vorum á þá virkaði kóðinn eða ekki. Í dag þarf því að byrja á að lagfæra það aðeins.

##### Markmið:
  Lagfæra þarf folders. Taka fund um LL og halda áfram með forritunina. Þessi dagbókafærsla er aðeins sein þar sem það gleymdist að skrifa hér á github en við mættum allir klukkan 10 og hófumst handa.
  
##### Staða eftir daginn:
  Búnir að lægfæra folder strúktúrinn þannig að allt sé keyranlegt. Einnig hafist handa á LL klösunum. 
  
  
### 06.12.2019

##### Ástand:
  Eftir stuttan fund vildi Gummi geta byrjað á Logic Layerinu. Til þess vildi hann að við myndum breyta Voyage og flights upp á nýtt. Vinnan heldur því áfram eins og venja er.
  
##### Markmið:
  Endurgera Voyage og flights. Byrja á LL og halda áfram með það sem þarf að gera.
  
##### Staða eftir daginn:
  Endurgerðum LL og model klasana og gerðum þá nánast fullkomna, löguðum helling af basic böggum og erum komnnir langt á leið.


### 09.12.2019

##### Ástand:
Guðmundur eyddi miklum tíma um helgina í að laga og betrumbæta kóðann. Eins og staðan er, er mikil virkni kominn, en það á enn þá eftir að útfæra flóknari virkni forritsins

##### Markmið:
Endurskrifa UI layer-inn til að geta sett upp curses svo hægt sé að stjórna kerfinu með örvatökkunum. Það þarf einnig að vinna með tíma og bæta við almennilegu ID kerfið

##### Staða eftir daginn:
Í dag náðum við að læra hvernig við vinnum með tíma, bættum við ID í IO og Data skjölin og héldum áfram að vinna í LL. Ýmir og Kristofer byrjuðu að læra hvernig við getum implementað Curses svo hægt sé að navigatea með örvunum og þeir byrjuðu á að skrifa smá handrit fyrir vídeóið. Bjartur fór aðeins í hönnunarskýrsluna og lagfærði hana aðeins.


### 10.12.2019

##### Ástand:
Kristofer eyddi smá tíma í gærkvöldi að leika sér með curses og bjó til smá prototýpu þannig að hann og Ýmir geta núna implementað curses í verkefnið þegar að það er tilbúið.

##### Markmið:
Halda áfram með LL meðal annars að laga Voyage og Flights, bæta við virkni í Voyages og Flights og geta mannað og búið til Voyages. Ýmir og Kristofer ætla að klára handritið sitt og byrja að taka upp myndbandið.

##### Staða eftir daginn:
Það er langt komið með voyages, mikil hugmyndarvinna en minni framkvæmd í flight klasann og í logic layerinum var mikil hugmyndarvinna og álíka mikil framkvæmd. Vídeóið og handritið var óklárað, en það verður byrjað á tökum á morgun.

### 11.12.2019

##### Ástand:
Voyage komið og erum að klára vinnu í A og B kröfum og byrjaðir að skipuleggja C kröfur. Ýmir og Kristofer eru að klára curses UI-ið og ætla þeir eftir það að hefja tökur myndskeiðsins.

##### Markmið:
Vera komnir langt með skýrsluna, klára A kröfur og hefja tökur myndskeiðsins.

##### Staða eftir daginn:
Nánast komnir með fullklárað forrit, þó eru nokkrar kröfur sem við eigum eftir að uppfylla. Undirbúningur fyrir myndbandið er búinn og þarf nú aðeins að taka það upp. Vinna í skýrslunni er komin langt á leið en hún er enn ókláruð.

### 12.12.2019

##### Ástand:
Ennþá nokkrir villur í forritinu sem á eftir að laga en því verður kipt í lag í dag. Allt undirbúið fyrir upptöku á myndbandinu og vinna í skýrslu er komið langt á leið.

##### Markmið:
Vera búnir með að forrita kerfið og eiga aðeins aflúsun og prófun eftir, vera búnir að taka upp meirihlutann af myndbandinu og nánast búnir meðs skýrsluna.

##### Staða eftir daginn:
Núna er hægt að sjá hvenær flugvélar eru lausar eftir ákveðinni dagsetningu. Voyage er 99% tilbúið. Núna þarf einungis að implementa flights, fínpússa kóðann og klára hönnunarskýrslu

### 13.12.2019

##### Ástand:
Það eina sem er eftir að gera er að fínpússa kóðann, klára nokkur logic, klára myndbandið og klára skýrsluna.

##### Markmið:
Klára verkefnið

##### Staða eftir daginn:
Við kláruðum verkefnið